﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EuropaUniversalis4ModMaker
{
    static class CapitalAssignment
    {
        static List<int> forbiddenProvinces = new List<int>();
        static int totalProvinces;

        static public void Init()
        {
            System.IO.StreamReader read = new System.IO.StreamReader(@"ForbiddenProvinces.txt");
            string[] filecontents = (read.ReadToEnd().Split(' '));
            for (int idx = 1; idx < filecontents.Length; idx++)
            {
                try
                {
                    forbiddenProvinces.Add(Convert.ToInt32(filecontents[idx]));
                }
                catch
                {
                    Console.WriteLine("ERROR: not a number");
                }
            }

            totalProvinces = 2002;


            Console.WriteLine("Loaded " + filecontents.Length + " forbiddenProvinces");
        }

        static public void Init(Map map)
        {
            foreach (Province province in map.seaProvinceList)
            {
                forbiddenProvinces.Add(province.provinceNo);
            }
            totalProvinces = map.provinceList.Count;
            

        }

        static public int AssignCapital()
        {
            int capital = Random.RandomInt(1, totalProvinces);

            for (int idx = 0; idx < forbiddenProvinces.Count; idx++)
            {
                if (capital == forbiddenProvinces[idx])
                {
                    AssignCapital();
                    idx = forbiddenProvinces.Count;
                }
            }

            return capital;
        }

        static public bool forbidden(int provinceId)
        {
            for (int idx = 0; idx < forbiddenProvinces.Count; idx++)
            {
                if (forbiddenProvinces[idx] == provinceId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
