﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class HistoryGenerator
    {
        List<ProvinceHistory> provinces = new List<ProvinceHistory>();

        public void GenerateHistory()
        {
            LoadProvinces();
            CreateAdjacencies();
        }
        protected void LoadProvinces()
        {
            StreamReader readFile;

            try
            {
                readFile = new StreamReader(Constants.MAP + "/definition.csv");
            }
            catch
            {
                readFile = new StreamReader(Constants.INSTALLPATH + "/map/definition.csv");
            }
            string line;
            while ((line = readFile.ReadLine()) != null)
            {
                int nextSemi = line.IndexOf(";");
                string provinceNo = line.Substring(0, nextSemi);
                int previousSemi = nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string red = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string green = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string blue = line.Substring(previousSemi, nextSemi);
                previousSemi += nextSemi + 1;
                nextSemi = line.IndexOf(";", previousSemi) - previousSemi;
                string provinceName = line.Substring(previousSemi, nextSemi);

                
                try
                {

                    ProvinceHistory province = new ProvinceHistory(Convert.ToInt32(provinceNo),
                        Color.FromArgb(Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue)),
                        provinceName);
                    provinces.Add(province);
                    Console.WriteLine("Added province " + provinceNo + " to history generator");
                }
                catch
                {
                    Console.WriteLine(provinceNo + " is not valid in the definitions file"); 
                }
            }
            Console.WriteLine("loaded all provinces to history generator");

        }

        protected void CreateAdjacencies()
        {
            Bitmap provinceMap;

            try
            {
                provinceMap = new Bitmap(Constants.MAP + "/provinces.bmp");
            }
            catch
            {
                provinceMap = new Bitmap(Constants.INSTALLPATH + "/map/provinces.bmp");
            }
            Color previousColor = provinceMap.GetPixel(0, 0);
            for (int idx = 0; idx < provinceMap.Width; idx++)
            {
                for (int jdx = 0; jdx < provinceMap.Height; jdx++)
                {
                    if (provinceMap.GetPixel(idx, jdx).ToArgb() != previousColor.ToArgb())
                    {
                        //need to check 
                        GetProvinceFromColor(provinceMap.GetPixel(idx, jdx)).AddAdjacency(GetProvinceFromColor(previousColor));
                        GetProvinceFromColor(previousColor).AddAdjacency(GetProvinceFromColor(provinceMap.GetPixel(idx, jdx)));
                    }
                }
                Console.WriteLine("Checked line " + idx + " for adjacenies");
            }
        }

        protected ProvinceHistory GetProvinceFromColor(Color checkColor)
        {
            foreach (ProvinceHistory province in provinces)
            {
                if (province.provinceColor.ToArgb() == checkColor.ToArgb())
                {
                    return province;
                }
            }

            return null;
        }
    }

    class ProvinceHistory
    {
        public int provinceNo { get; private set; }
        public Color provinceColor { get; private set; }
        public string provinceName { get; private set; }
        List<ProvinceHistory> Adjacencies = new List<ProvinceHistory>();

        public ProvinceHistory(int provinceNo, Color provinceColor, string provinceName)
        {
            this.provinceNo = provinceNo;
            this.provinceColor = provinceColor;
            this.provinceName = provinceName;
        }
        public void AddAdjacency(ProvinceHistory otherprovince)
        {
            bool alreadyRecorded = false;
            int idx = 0;
            while (!alreadyRecorded || idx < Adjacencies.Count)
            {
                if (Adjacencies[idx].provinceNo == otherprovince.provinceNo)
                {
                    alreadyRecorded = true;
                    Console.WriteLine(provinceNo + "is adjacent to " + otherprovince.provinceNo);
                }
            }

            if (!alreadyRecorded)
            {
                Adjacencies.Add(otherprovince);
            }
        }

    }
}
