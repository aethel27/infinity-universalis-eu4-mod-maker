﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    public struct PlColour
    {
        public int R;
        public int G;
        public int B;
    }

    class Tag
    {
        public string Id { get; protected set; }
        public string countryName { get; protected set; }
        public string countryCulture { get; protected set; }

        public PlColour nationalColor;

        //colors for the country units. 
         PlColour unitColor1;
         PlColour unitColor2;
         PlColour unitColor3;

         public string graphicalCulture { get; protected set; }

         public int capitalProvince; 


        public Tag(string tagId)
        {
            Id = tagId;
            Console.WriteLine("New tag created:" + tagId);
            GenerateName();
            GenerateColors();
        }
        public Tag(string tagID, string countryName, int red, int green, int blue)
        {
            Id = tagID;
            this.countryName = countryName;
            nationalColor.R = red;
            nationalColor.G = green;
            nationalColor.B = blue;
        }


        protected void GenerateName()
        {
            countryName = NameGenerator.GenerateName();

            //generate the countries culture. 
            float percentile;
            if (!(countryName[countryName.Length - 1].Equals("a") ||
                countryName[countryName.Length - 1].Equals("e") ||
                countryName[countryName.Length - 1].Equals("i") ||
                countryName[countryName.Length - 1].Equals("o") ||
                countryName[countryName.Length - 1].Equals("u")))
            {
                percentile = Random.RandomFloat();

                if (percentile < 0.33f)
                {
                    countryCulture = countryName + "ish";
                }
                else if (percentile >= 0.33 && percentile < 0.66f)
                {
                    countryCulture = countryName + "ian";
                }
                else
                {
                    countryCulture = countryName + "ic";
                }


            }
            else
            {
                countryCulture = countryName;
            }
            Console.WriteLine("Culture of " + Id + " is: " + countryCulture);


            //add any extra words that a country might have
            percentile = Random.RandomFloat();
            if (percentile > 0.90f)
            {
                countryName += "land";
            }

            if (percentile > 0.85 && percentile <= 0.92)
            {
                countryName += "_Republic";
            }
            else if (percentile > 0.80 && percentile <= 0.85 || percentile > 0.99)
            {
                countryName += "_Empire";
            }


            Console.WriteLine("Name of " + Id + " is: " + countryName);

            
        }

        protected void GenerateColors()
        {
            nationalColor.R = Random.RandomInt(0, 256);
            nationalColor.G = Random.RandomInt(0, 256);
            nationalColor.B = Random.RandomInt(0, 256);

            unitColor1.R = Random.RandomInt(0, 256);
            unitColor1.G = Random.RandomInt(0, 256);
            unitColor1.B = Random.RandomInt(0, 256);

            unitColor2.R = Random.RandomInt(0, 256);
            unitColor2.G = Random.RandomInt(0, 256);
            unitColor2.B = Random.RandomInt(0, 256);

            unitColor3.R = Random.RandomInt(0, 256);
            unitColor3.G = Random.RandomInt(0, 256);
            unitColor3.B = Random.RandomInt(0, 256);
        }

        public void WriteUnitColors(StreamWriter output)
        {
            output.WriteLine("# " + countryName);
            output.WriteLine(Id + " = {");
            output.WriteLine("  color1= {" + unitColor1.R + " " + unitColor1.G + " " + unitColor1.B + "}");
            output.WriteLine("  color2= {" + unitColor2.R + " " + unitColor2.G + " " + unitColor2.B + "}");
            output.WriteLine("  color3= {" + unitColor3.R + " " + unitColor3.G + " " + unitColor3.B + "}");
            output.WriteLine("}");
            output.WriteLine();
            output.Flush();
        }

        public void GenerateAndWriteCountryFile(List<List<string>> UnitLists)
        {
            //create the file. 
            StreamWriter file;
            try
            {
                file = File.CreateText(Constants.COMMON + "/countries/" + countryName + ".txt");
            }
            catch (IOException e)
            {
                try
                {
                    file = new StreamWriter(Constants.COMMON + "/countries/" + countryName + ".txt");
                }
                catch (IOException e2)
                {
                    Console.WriteLine("ERROR: could not create " + countryName + ".txt");
                    return;
                }
            }
            file.WriteLine("# country name: " + countryName);
            file.WriteLine();
            
            //write the graphic culture. 
            GenerateGraphicalCulture();
            file.WriteLine("graphical_culture = " + graphicalCulture);
            file.WriteLine();

            //write the color
            file.WriteLine("color= {" + nationalColor.R + " " + nationalColor.G + " " + nationalColor.B + "}");
            file.WriteLine();

            //add in a historic score for flavor. 
            if (Random.RandomFloat() > 0.90f)
            {
                file.WriteLine("historical_score = " + Random.RandomInt(0, 9000));
                file.WriteLine();
            }

            //write ideas. 
            file.WriteLine("historical_idea_groups = {");
	        file.WriteLine(" defensive_ideas");
	        file.WriteLine(" religious_ideas");
            file.WriteLine("	trade_ideas");
            file.WriteLine("	economic_ideas");
            file.WriteLine("	exploration_ideas");
            file.WriteLine("	innovativeness_ideas");
            file.WriteLine("	offensive_ideas");
            file.WriteLine(	"administrative_ideas");
            file.WriteLine("}");
            file.WriteLine();
            file.Flush();

            //write units. 
            file.WriteLine("historical_units = {");
            file.WriteLine("    " + UnitLists[0][0]);
            for (int idx = 0; idx < UnitLists.Count; idx++)
            {
                if (UnitLists[idx].Count > 0)
                {
                    int firstrandom = Random.RandomInt(0, UnitLists[idx].Count);
                    int secondRandom = Random.RandomInt(0, UnitLists[idx].Count);

                    if (firstrandom < secondRandom)
                    {
                        file.WriteLine("    " + UnitLists[idx][firstrandom]);
                        file.WriteLine("    " + UnitLists[idx][secondRandom]);
                    }
                    else
                    {
                        file.WriteLine("    " + UnitLists[idx][secondRandom]);
                        file.WriteLine("    " + UnitLists[idx][firstrandom]);
                    }
                    file.Flush();
                }
            }

            /*file.WriteLine("	western_medieval_infantry");
            file.WriteLine("	western_medieval_knights");
            file.WriteLine("	western_men_at_arms");
            file.WriteLine("	spanish_tercio");
            file.WriteLine("	french_caracolle");
            file.WriteLine("	austrian_tercio");
            file.WriteLine("	austrian_grenzer");
            file.WriteLine("	austrian_hussar");
            file.WriteLine("	austrian_white_coat");
            file.WriteLine("	austrian_jaeger");
            file.WriteLine("	mixed_order_infantry");
            file.WriteLine("	open_order_cavalry");
            file.WriteLine("	napoleonic_square");
            file.WriteLine("	napoleonic_lancers");*/
            file.WriteLine("}");
            file.WriteLine();
            file.Flush();

            //Monarch Names
            file.WriteLine("monarch_names = {");
            int monarchNo = Random.RandomInt(10, 30);
            for (int idx = 0; idx < monarchNo; idx++)
            {
                file.WriteLine('"' + NameGenerator.GenerateName() + " #" + Random.RandomInt(0, 10) + '"' + " = " + Random.RandomInt(-80, 80));
                file.Flush();
            }
            file.WriteLine("}");
            file.WriteLine(); 

            //Leader Names
            file.WriteLine("leader_names = {");
            int leaderNo = Random.RandomInt(10, 30);
            for (int idx = 0; idx < leaderNo; idx++)
            {
                file.WriteLine(NameGenerator.GenerateName());
                file.Flush();
            }
            file.WriteLine("}");
            file.WriteLine();

            //ship names
            file.WriteLine("ship_names = {");
            int shipNo = Random.RandomInt(10, 30);
            for (int idx = 0; idx < shipNo; idx++)
            {
                file.WriteLine(NameGenerator.GenerateName());
                file.Flush();
            }
            file.WriteLine("}");
            file.WriteLine();

            //Army Names
            file.WriteLine("army_names = {");

            float percentile = Random.RandomFloat();
            if (percentile <= 0.10)
            {
                file.WriteLine('"' +"Armee von $PROVINCE$" + '"');
            }
            else if (percentile <= 0.30 && percentile > 0.10)
            {
                file.WriteLine('"' + "Army of $PROVINCE$" + '"');
            }
            else if (percentile <= 0.60 && percentile > 0.30)
            {
                file.WriteLine('"' + "Legion of $PROVINCE$" + '"');
            }
            else if (percentile <= 0.70 && percentile > 0.60)
            {
                file.WriteLine('"' + "Armata di $PROVINCE$" + '"');
            }
            else if (percentile <= 0.75 && percentile > 0.70)
            {
                file.WriteLine('"' + "Blitzkriegen $PROVINCE$" + '"');
            }

            file.WriteLine("}");

            file.Close();
            Console.WriteLine("Country file for " + countryName + " written");

        }
        protected void GenerateGraphicalCulture()
        {
            switch (Random.RandomInt(0, 9))
            {
                case 0: graphicalCulture = "westerngfx"; break;
                case 1: graphicalCulture = "easterngfx"; break;
                case 2: graphicalCulture = "muslimgfx"; break;
                case 3: graphicalCulture = "indiangfx"; break;
                case 4: graphicalCulture = "asiangfx"; break;
                case 5: graphicalCulture = "africangfx"; break;
                case 6: graphicalCulture = "northamericagfx"; break;
                case 7: graphicalCulture = "southamericagfx"; break;
                case 8: graphicalCulture = "inuitgfx"; break;
            }
        }

        public void GenerateHistoryFile(string techGroup)
        {
            StreamWriter file;
            file = File.CreateText(Constants.HISTORY + "/countries/" + Id +" - " + countryName + ".txt");
            Console.WriteLine("writing history file for " + countryName);

            switch (Random.RandomInt(1, 3))
            {
                case 1: file.WriteLine("government = tribal_despotism"); break;
                case 2: file.WriteLine("government = tribal_federation"); break;
                case 3: file.WriteLine("government = tribal_democracy"); break;
            }

            file.WriteLine("mercantilism = 0.1");
            file.WriteLine("technology_group = " + techGroup);
            file.WriteLine("primary_culture = " + countryCulture);
            file.WriteLine("religion = animism");
            file.WriteLine("capital = " + capitalProvince);   //175	# Bayonne

            

            //create a monarch. 
            file.WriteLine();
            file.WriteLine();
            file.WriteLine(Constants.STARTDATE + ".1.2 = {");
            file.WriteLine("    monarch = {");
            file.WriteLine("        name = " + '"' + NameGenerator.GenerateName() + '"');
            file.WriteLine("        dynasty = " + '"' + NameGenerator.GenerateName() + '"');
            file.WriteLine("        adm = " + Random.RandomInt(0, 6));
            file.WriteLine("        dip = " + Random.RandomInt(0, 6));
            file.WriteLine("        mil = " + Random.RandomInt(0, 6));
            file.WriteLine("    }");
            file.WriteLine("}");
            file.Flush();
            file.Close();

        }
    }

    
}
