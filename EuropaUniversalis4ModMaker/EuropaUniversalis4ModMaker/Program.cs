﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class Program
    {
        static void Main(string[] args)
        {
            //settings for generator. 
            int RANDOMSEED = 9999; //seed used for Tribalis Universalis
            bool GENERATENEWMAP = false;


            //seed the random. 
            Random.Init(RANDOMSEED);
            //Initialize the name generator
            NameGenerator.Init();
            //inti the constants.
            string modName;
           // modName = "Tribalis_Universalis";
            modName = "testing";
            Constants.Init(modName, "C:/Users/thegreatpl/Documents/Paradox Interactive/Europa Universalis IV/mod", 
                "C:/Program Files (x86)/Steam/steamapps/common/Europa Universalis IV"); 
            //init the forbidden provinces to prevent any sea or wasteland province being assigned. 
            


            StreamWriter modFileStream;
            modFileStream = File.CreateText(Constants.PATH + "/" + Constants.NAME + ".mod");

            modFileStream.WriteLine("name=" + Constants.NAME);
            modFileStream.WriteLine("path=" + "mod" + "/" + Constants.NAME);
            modFileStream.WriteLine("tags=");
            modFileStream.WriteLine("{");
            modFileStream.WriteLine("   " + '"' + "Alternate History" + '"');
            modFileStream.WriteLine("   " + '"' + "Gameplay" + '"');
            modFileStream.WriteLine("}");

            string[] missionFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/missions");

            foreach (string mission in missionFileNames)
            {
                modFileStream.WriteLine("replace_path=" + '"' + "/missions" + Path.GetFileName(mission) + '"');
            }



            modFileStream.Close();
            Console.WriteLine("Mod file created");

            //create Directories
            System.IO.Directory.CreateDirectory(Constants.MODPATH);
            Console.WriteLine("Mod Directory created");

            System.IO.Directory.CreateDirectory(Constants.COMMON);
            Console.WriteLine("Common Directory created");

            System.IO.Directory.CreateDirectory(Constants.LOCAL);
            Console.WriteLine("localisation directory created");

            System.IO.Directory.CreateDirectory(Constants.GFX);
            Console.WriteLine("GFX directory created");

            System.IO.Directory.CreateDirectory(Constants.HISTORY);
            Console.WriteLine("History directory created");

            System.IO.Directory.CreateDirectory(Constants.EVENTS);
            Console.WriteLine("Events directory created");

            System.IO.Directory.CreateDirectory(Constants.MISSIONS);
            Console.WriteLine("Missions directory created");

            System.IO.Directory.CreateDirectory(Constants.DECISIONS);
            Console.WriteLine("Decisions directory created");

            System.IO.Directory.CreateDirectory(Constants.MAP);
            Console.WriteLine("Map Directory Created");


            System.IO.Directory.CreateDirectory(Constants.COMMON + "/triggered_modifiers");
            Console.WriteLine("Creating triggered modifiers folder");

            System.IO.Directory.CreateDirectory(Constants.COMMON + "/ideas");
            Console.WriteLine("Creating ideas directory");

            System.IO.Directory.CreateDirectory(Constants.UNITS);
            Console.WriteLine("Creating unit directory");

            System.IO.Directory.CreateDirectory(Constants.COMMON + "/technologies");
            Console.WriteLine("Creating technology directory");

            System.IO.Directory.CreateDirectory(Constants.COMMON + "/bookmarks");
            Console.WriteLine("Created bookmarks directory");

            System.IO.Directory.CreateDirectory(Constants.COMMON + "/countries");
            Console.WriteLine("Created common countries directory");

            System.IO.Directory.CreateDirectory(Constants.HISTORY + "/countries");
            Console.WriteLine("Creating history countries directory");

            //redefine defines. 
            StreamWriter defines = File.CreateText(Constants.COMMON + "/defines.lua");
            StreamReader oldDefines = new StreamReader(Constants.INSTALLPATH + "/common/defines.lua");
            string line;
            while ((line = oldDefines.ReadLine()) != null)
            {
                if (line.Contains("START_DATE"))
                {
                    defines.WriteLine(line.Substring(0, line.IndexOf("=")) + " = " + '"' + Constants.STARTDATE + ".2.2" + '"' + ",");
                }
                else if (line.Contains("END_DATE"))
                {
                    defines.WriteLine(line.Substring(0, line.IndexOf("=")) + " = " + '"' + Constants.ENDDATE + ".11.11" + '"' + ",");
                }
                else
                {
                    defines.WriteLine(line);
                    defines.Flush();
                }
            }



            StreamWriter newTriggers;
            newTriggers = File.CreateText(Constants.COMMON + "/triggered_modifiers/00_triggered_modifiers.txt");

            StreamReader oldTriggers = new StreamReader(Constants.INSTALLPATH + "/common/triggered_modifiers/00_triggered_modifiers.txt");
            while ((line = oldTriggers.ReadLine()) != null)
            {
                newTriggers.WriteLine(line);
                newTriggers.Flush();
            }
            oldTriggers.Close();

            newTriggers.WriteLine("explore_trigger = {");
            newTriggers.WriteLine("     potential = { ");
            newTriggers.WriteLine("         OR = {");
            newTriggers.WriteLine("             government = tribal_despotism");
            newTriggers.WriteLine("             government = tribal_federation");
            newTriggers.WriteLine("             government  = tribal_democracy");
            newTriggers.WriteLine("         }");
            newTriggers.WriteLine("         NOT = { num_of_cities = 3 }");
            newTriggers.WriteLine("     }");
            newTriggers.WriteLine("     trigger = {    ");
            newTriggers.WriteLine("         OR = {");
			newTriggers.WriteLine("             government = tribal_despotism");
            newTriggers.WriteLine("             government = tribal_federation");
			newTriggers.WriteLine("             government  = tribal_democracy");
            newTriggers.WriteLine("         }");
            newTriggers.WriteLine("         NOT = { num_of_cities = 3 }");
            newTriggers.WriteLine("     }");
            newTriggers.WriteLine("     may_explore = yes"); //this does not work. 
            newTriggers.WriteLine("     colonists = 1");
            newTriggers.WriteLine("     technology_cost = 1.0");
            newTriggers.WriteLine("     global_tax_modifier = 0.5");
            newTriggers.WriteLine("     global_colonial_growth = 100");
            newTriggers.WriteLine("}");
            newTriggers.Flush();

            newTriggers.WriteLine("expansionist_tribe = {");
            newTriggers.WriteLine("     potential = { ");
            newTriggers.WriteLine("         OR = {");
            newTriggers.WriteLine("             government = tribal_despotism");
            newTriggers.WriteLine("             government = tribal_federation");
            newTriggers.WriteLine("             government  = tribal_democracy");
            newTriggers.WriteLine("         }");
            newTriggers.WriteLine("     }");
            newTriggers.WriteLine("     trigger = {    ");
            newTriggers.WriteLine("         OR = {");
            newTriggers.WriteLine("             government = tribal_despotism");
            newTriggers.WriteLine("             government = tribal_federation");
            newTriggers.WriteLine("             government  = tribal_democracy");
            newTriggers.WriteLine("         }");
            newTriggers.WriteLine("     }"); 
            newTriggers.WriteLine("     colonists = 1");
            newTriggers.WriteLine("     global_colonial_growth = 100");
            newTriggers.WriteLine("}");

            newTriggers.Flush();
            newTriggers.Close();
            Console.WriteLine("New Colonizing trigger created");


            StreamWriter defualtIdeasOutput;
            defualtIdeasOutput = File.CreateText(Constants.COMMON + "/ideas/zzz_default_idea.txt");
            StreamReader defaultIdeasInput = new StreamReader(Constants.INSTALLPATH + "/common/ideas/zzz_default_idea.txt");
            bool startBlock = false;
            while ((line = defaultIdeasInput.ReadLine()) != null)
            {
                if (startBlock == false)
                {
                    if (line.Contains("start ="))
                    {
                        startBlock = true;
                    }
                    defualtIdeasOutput.WriteLine(line);
                    defualtIdeasOutput.Flush();
                }
                else if (startBlock)
                {
                    defualtIdeasOutput.WriteLine("          may_explore = yes");
                    //defualtIdeasOutput.WriteLine(line);
                    defualtIdeasOutput.Flush();
                    startBlock = false;
                }
            }
            defaultIdeasInput.Close();
            defualtIdeasOutput.Close();

          /*  StreamWriter colonialMission = File.CreateText(Constants.MISSIONS + "/plColonial.txt");

            colonialMission.WriteLine("establish_neighbor_colony = {");
            colonialMission.WriteLine("type = neighbor_provinces");
            colonialMission.WriteLine("category = DIP");
            colonialMission.WriteLine("ai_mission = yes");
            colonialMission.WriteLine("allow = {");
            colonialMission.WriteLine("has_discovered = FROM");
            colonialMission.WriteLine("is_empty = yes");
            colonialMission.WriteLine("}");
            colonialMission.WriteLine("abort = { is_empty = no } ");
            colonialMission.WriteLine("success = { owned_by = FROM } ");
            colonialMission.WriteLine("chance = { factor = 1000 }");
            colonialMission.WriteLine("effect = { FROM = { add_country_modifier = { name = "
                + '"' + "colonial_enthusiasm" + '"' + "duration = 1825 } } }");
            colonialMission.WriteLine("}");

            colonialMission.Flush();
            colonialMission.Close();*/

            //sort out the lack of trade goods. 
            StreamWriter tradeEvents;
            tradeEvents = File.CreateText(Constants.EVENTS + "/TradeGoods.txt");

            StreamReader oldTrade = new StreamReader(Constants.INSTALLPATH + "/events/TradeGoods.txt");
            //string previousLine = "#File has changed to make sure trade goods can be gained in europe and asia";
            //bool deleteHanging = false;
            while ((line = oldTrade.ReadLine()) != null)
            {
                if (line.Contains("region =") || line.Contains("province_id"))
                {
                    tradeEvents.WriteLine("             trade_goods = unknown");
                   /* if (previousLine.Contains("}"))
                    {
                        //tradeEvents.WriteLine(previousLine);
                    }
                    else if (previousLine.Contains("modifier = {"))
                    {
                        previousLine = "";
                    }
                    else
                    {
                        deleteHanging = true;
                    }
                }
                else if (deleteHanging)
                {
                    previousLine = "";
                    deleteHanging = false;*/
                }
                else
                {
                    tradeEvents.WriteLine(line);
                    //previousLine = line;
                    tradeEvents.Flush();
                }
            }
            tradeEvents.WriteLine(line);
            oldTrade.Close();
            tradeEvents.Close();

            StreamWriter colonialEvent = File.CreateText(Constants.EVENTS + "/plColonial.txt");

            colonialEvent.WriteLine("namespace = plcolonial");
            colonialEvent.WriteLine("province_event = {");
            colonialEvent.WriteLine("   id = plcolonial.1");
            colonialEvent.WriteLine("   title = " + '"' + "EVTNAME6138" + '"');
            colonialEvent.WriteLine("   desc = " + '"' + "EVTDESC6138" + '"');
            colonialEvent.WriteLine("   picture = COLONIZATION_eventPicture");
            colonialEvent.WriteLine();
            colonialEvent.WriteLine();
            colonialEvent.WriteLine("   trigger = {");
            colonialEvent.WriteLine("       owner = {");
            colonialEvent.WriteLine("           ai = yes");
            colonialEvent.WriteLine("           NOT = { num_of_colonies = 1 }");
            colonialEvent.WriteLine("           colonists = 1");
            colonialEvent.WriteLine("           }");
            colonialEvent.WriteLine("       has_empty_adjacent_province = yes");
            colonialEvent.WriteLine("       is_colony = no");
            colonialEvent.WriteLine("   }");
            colonialEvent.WriteLine("   mean_time_to_happen = {");
            colonialEvent.WriteLine("       days = 10");
            colonialEvent.WriteLine("   }");
            colonialEvent.WriteLine("   option = {");
            colonialEvent.WriteLine("       name = " + '"' + "EVTOPTA6138" + '"');
            colonialEvent.WriteLine("       random_empty_neighbor_province = {");
            colonialEvent.WriteLine("           create_colony = 2");
            colonialEvent.WriteLine("       }");
            colonialEvent.WriteLine("   }");
            colonialEvent.WriteLine("}");
            colonialEvent.Flush();
            colonialEvent.Close();

            Console.WriteLine("Created colonization event file");

            
            
            TagGeneration tagGenner = new TagGeneration();
            Technology techTree = new Technology();
            Map mapGenner;
            HistoryGenerator history = new HistoryGenerator();

            if (GENERATENEWMAP)
            {
                mapGenner = new Map();
                mapGenner.GenerateNewMap();
                CapitalAssignment.Init(mapGenner);
            }
            else
            {
                CapitalAssignment.Init();
            }

            for (int idx = 0; idx < 200; idx++)
            {
                tagGenner.GenerateTag();
            }
            Console.WriteLine("Tags Generated");

            Console.WriteLine("Starting Tech Tree generation");
            techTree.GenerateTechTree();

            //history.GenerateHistory();

            tagGenner.WriteTags();
            tagGenner.WriteCountryColors();
            tagGenner.WriteCountries(techTree.UnitsLists);
            tagGenner.WriteHistoryFiles(techTree);

            
           /* if (GENERATENEWMAP)
            {
                tagGenner.WriteProvinceHistory(mapGenner); 
            }
            else
            {*/
                tagGenner.WriteProvinceHistory();
            //}

            tagGenner.CreateFlags();
            tagGenner.WriteTagLocalization();
            tagGenner.WriteCultureFile();
            tagGenner.WriteProvinceLocalization();
            

            string[] bookmarkfilenames = Directory.GetFiles(Constants.INSTALLPATH + "/common/bookmarks");

            foreach (string bookmark in bookmarkfilenames)
            {
                File.CreateText(Constants.COMMON + "/bookmarks/" + Path.GetFileName(bookmark));
            }

            StreamWriter startBookmark = File.CreateText(Constants.COMMON + "/bookmarks/Dawn_of_civilization.txt");

            startBookmark.WriteLine("bookmark = {");
            startBookmark.WriteLine("   name = " + '"' + "DAWN_NAME" + '"');
            startBookmark.WriteLine("   desc = " + '"' + "DAWN_DESC" + '"');
            startBookmark.WriteLine("   date = " + Constants.STARTDATE + ".2.2");
            startBookmark.WriteLine("}");
            startBookmark.Flush();
            startBookmark.Close();

            Console.WriteLine(Constants.NAME + " has been generated");
            Console.ReadKey();
            //System.Threading.Thread.Sleep(100);
            //System.IO.File.WriteAllLines(PATH + "/" + NAME + ".mod", modFile);
        }
    }

    static class Constants
    {
        static public string NAME { get; private set;}
        static public string PATH { get; private set;} 
        static public string MODPATH {get; private set;}
        static public string INSTALLPATH {get; private set;}

        static public string COMMON { get; private set; }
        static public string HISTORY { get; private set; }
        static public string GFX { get; private set; }
        static public string LOCAL { get; private set; }
        static public string EVENTS { get; private set; }
        static public string MISSIONS { get; private set; }
        static public string DECISIONS { get; private set; }
        static public string MAP { get; private set; }

        static public string UNITS { get; private set; }

        static public int ENDDATE { get; private set; }
        static public int STARTDATE { get; private set; }
        
        static public void Init(string name, string path, string installPath)
        {
            NAME = name;
            PATH = path;
            INSTALLPATH = installPath;

            MODPATH = PATH + "/" + NAME;
            COMMON = MODPATH + "/common";
            HISTORY = MODPATH + "/history";
            GFX = MODPATH + "/gfx";
            LOCAL = MODPATH + "/localisation";
            EVENTS = MODPATH + "/events";
            MISSIONS = MODPATH + "/missions";
            DECISIONS = MODPATH + "/decisions";
            MAP = MODPATH + "/map";

            UNITS = COMMON + "/units";

            ENDDATE = 194136;
            STARTDATE = 1;

        }
       
        
    }
}
