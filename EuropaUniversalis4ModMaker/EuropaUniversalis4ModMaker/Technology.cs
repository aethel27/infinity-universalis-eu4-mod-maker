﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class Technology
    {
        private List<List<string>> Units = new List<List<string>>();

        public List<List<string>> UnitsLists { get { return Units; } }

        public List<string> techGroups { get; private set; }

        //lists of things for the generation of techs. 
        protected List<string> MaterialLevels = new List<string>();
        //used by mil tech. 
        protected List<Weapon> trainingLevel = new List<Weapon>();
        protected List<Weapon> armorNames = new List<Weapon>();
        protected List<Weapon> weaponNames = new List<Weapon>();
        protected List<List<Weapon>> weaponsTechLevelCheck = new List<List<Weapon>>();
        protected List<List<Weapon>> armourTechLevelCheck = new List<List<Weapon>>();

        StreamWriter localizationStream;
        StreamWriter unitLocalizationStream;

        int MINYEARSBETWEENTECHS = 10;
        int MAXYEARSBETWEENTECHS = 20;

        //mil tech variables
        int maxManeuver = 10;

        int currentMaterialLevel = 0;
        int currentTechNo = 1;
        int currentYear = Constants.STARTDATE;

        int currentManeuver = 1;
        int currentArmorForThisAge = 0;
        int currentTrainingLevel = 0;
        int currentSergeantsTech = 1;
        int currentShockTech = 1;

        //adm tech variables
        int currentProductionTech = 1;
        int currentSupplyTech = 1;
        int currentRoadsTech = 1;
        List<string> buildingNames = new List<string>();
        int currentBuildings = 0;
       

        //dip tech variables
        int currentTradeTech = 1;
        int currentRangeTech = 1;
        int currentNavalTech = 1;

        public void GenerateTechTree()
        {
            //create list of what the weapons and tools will be made out of at each level. 
            Console.WriteLine("Creating Material levels");
            MaterialLevels.Add("wooden");
            MaterialLevels.Add("flint");
            MaterialLevels.Add("copper");
            MaterialLevels.Add("bronze");
            MaterialLevels.Add("iron");
            MaterialLevels.Add("steel");
            MaterialLevels.Add("damascus_steel");
            MaterialLevels.Add("titanium");
            MaterialLevels.Add("adamantium");
            MaterialLevels.Add("carbon_nanotubes");

            //start the localization streams up. 
            localizationStream = File.CreateText(Constants.LOCAL + "/technology_l_english.yml");
            unitLocalizationStream = File.CreateText(Constants.LOCAL + "/units_l_english.yml");

            unitLocalizationStream.WriteLine("l_english:");

            //copy all old localizations apart from tech names. 
            StreamReader oldLocalization = new StreamReader(Constants.INSTALLPATH + "/localisation/technology_l_english.yml");
            string line;
            while ((line = oldLocalization.ReadLine()) != null)
            {
                //make sure not a tech name or description
                if (!(line.Contains("mil_tech_") || line.Contains("adm_tech_") || line.Contains("dip_tech_")))
                {
                    localizationStream.WriteLine(line);
                    localizationStream.Flush();
                }
            }
            //write in exceptions to above rule. 
            localizationStream.WriteLine(" mil_tech_short: " + '"' + "Mil Tech" + '"');
            localizationStream.WriteLine(" adm_tech_short: " + '"' + "Adm Tech" + '"');
            localizationStream.WriteLine(" dip_tech_short: " + '"' + "Dip Tech" + '"');
            localizationStream.Flush();

            oldLocalization.Close();


            GenerateTechGroups();
            GenerateMilTech();
            GenerateAdmTech();
            GenerateDipTech();


            localizationStream.Close();
            unitLocalizationStream.Close();
        }

        protected void GenerateTechGroups()
        {
            techGroups = new List<string>();

            StreamWriter technologyStream;
            technologyStream = File.CreateText(Constants.COMMON + "/technology.txt");
            

            //pull tags from the old list, as there is too many files they are linked in to replace for now. 
            StreamReader oldTechGroups = new StreamReader(Constants.INSTALLPATH + "/common/technology.txt");
            string line;
            bool groupsSectionStarted = false;
            while ((line = oldTechGroups.ReadLine()) != null)
            {
                if (!groupsSectionStarted)
                {
                    technologyStream.WriteLine(line);
                    technologyStream.Flush();

                    if (line.Contains("groups = {"))
                    {
                        groupsSectionStarted = true;
                    }
                }
                else
                {
                    int noOfNewTechGroups = Random.RandomInt(1, 20);
                    for (int idx = 0; idx < noOfNewTechGroups; idx++)
                    {
                        string techGroupName = NameGenerator.GenerateName();
                        Console.WriteLine("Generating tech group " + techGroupName);
                        techGroups.Add(techGroupName);
                        technologyStream.WriteLine("    " + techGroupName + " = {");
                        float modifier = Random.RandomFloat(-0.20f, 0.20f);
                        technologyStream.WriteLine("        modifier = " + modifier.ToString("0.00"));
                        technologyStream.WriteLine("        start_Level = 1");
                        float cavToInf = (Random.RandomFloat(0.01f, 0.10f) * 10);
                        technologyStream.WriteLine("        cav_to_inf_ratio = " + cavToInf.ToString("0.00"));
                        technologyStream.WriteLine("        power = " + Random.RandomInt(-3, 3));
                        technologyStream.WriteLine("    }");
                        technologyStream.Flush();
                    }
                    technologyStream.WriteLine(line);
                    technologyStream.Flush();
                    groupsSectionStarted = false;

                }

                
            }
            Console.WriteLine("technology file generated");
            oldTechGroups.Close();
            technologyStream.Close();
        }

        protected void GenerateMilTech()
        {
            
            
            StreamWriter miltechFile = File.CreateText(Constants.COMMON + "/technologies/mil.txt");

            //create a list of weapons each tech level may have. 
            
            weaponNames.Add(new Weapon("club", 1, 0, 0, 0, 2, 0));
            weaponNames.Add(new Weapon("short_sword", 2, 0, 0, 0, 0, 0));
            weaponNames.Add(new Weapon("spear", 1, 0, 0, 0, 2, 2));
            weaponNames.Add(new Weapon("javalin", 1, 0, 2, 2, 0, 2));
            weaponNames.Add(new Weapon("short_bow", 1, 0, 2, 0, 0, 0));
            weaponNames.Add(new Weapon("rapier", 2, 2, 0, 0, 2, 2));
            weaponNames.Add(new Weapon("long_bow", 1, 0, 3, 0, 0, 0));

            
            for (int idx = 0; idx < MaterialLevels.Count; idx++)
            {
                weaponsTechLevelCheck.Add(new List<Weapon>(weaponNames));
                foreach (Weapon weapon in weaponsTechLevelCheck[idx])
                {
                    weapon.SetMaterialLevel(idx);
                }
            }
            
            armorNames.Add(new Weapon("unarmored", 0, 0, 0, 0, 0, 0));
            armorNames.Add(new Weapon ("shield", 0, 1, 0, 0, 0, 0));
            armorNames.Add(new Weapon("chain", 0, 1, 0, 1, 0, 1));
            armorNames.Add(new Weapon("plate", 0, 1, 0, 1, 0, 2));

            
            for (int idx = 0; idx < MaterialLevels.Count; idx++)
            {
                armourTechLevelCheck.Add(armorNames);
                foreach (Weapon weapon in armourTechLevelCheck[idx])
                {
                    weapon.SetMaterialLevel(idx);
                }
            }

            
            trainingLevel.Add(new Weapon("conscripts", 0, 0, 0, 0, 0, 0));
            trainingLevel.Add(new Weapon("militia", 0, 1, 0, 0, 0, 1));
            trainingLevel.Add(new Weapon("warriors", 1, 1, 0, 0, 0, 0));
            trainingLevel.Add(new Weapon("master_warriors", 1, 1, 0, 0, 1, 1));
            trainingLevel.Add(new Weapon("soldiers", 1, 1, 0, 1, 1, 1));
            
            //create Units list. 
            for (int idx = 0; idx < MaterialLevels.Count; idx++)
            {
                Units.Add(new List<string>());
            }

            //set up variables. 
            

            //write out base tech level 
            miltechFile.WriteLine("monarch_power = MIL");
            miltechFile.WriteLine();
            miltechFile.WriteLine();
            miltechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
            miltechFile.WriteLine("technology = {");
            miltechFile.WriteLine("     # tech " + currentTechNo);
            miltechFile.WriteLine("     year = " + currentYear);
            miltechFile.WriteLine("     sprite_level = 1");
            miltechFile.WriteLine("     infantry_fire = 0.10");
            miltechFile.WriteLine("     infantry_shock = 0.10");
            miltechFile.WriteLine("     land_morale = 0.5");
            miltechFile.WriteLine("     enable = " + EnableNewUnit(0));
            currentTechNo++;

            miltechFile.WriteLine("}");
            miltechFile.Flush();

            int techsToNextEpoch = 10;

            while (currentYear < Constants.ENDDATE)
            {
                if (techsToNextEpoch < 0 && currentMaterialLevel < MaterialLevels.Count-1)
                {
                    
                        currentMaterialLevel++;
                        currentArmorForThisAge = 0;

                        techsToNextEpoch = Random.RandomInt(20, 100);
                        techsToNextEpoch *= currentMaterialLevel;
                        miltechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
                    
                }
                currentYear += Random.RandomInt(MINYEARSBETWEENTECHS, MAXYEARSBETWEENTECHS);
                miltechFile.WriteLine("technology = {");
                miltechFile.WriteLine("     # tech " + currentTechNo);
                miltechFile.WriteLine("     year = " + currentYear);
                miltechFile.Flush();
                GenerateMilTechEntry(miltechFile);
                miltechFile.WriteLine("}");

                
                currentTechNo++;
                techsToNextEpoch--;
            }

            miltechFile.Flush();
            miltechFile.Close();
        }

        protected void GenerateMilTechEntry(StreamWriter milTechFile)
        {
            switch (Random.RandomInt(1, 3))
            {
                case 1:
                    //
                    #region Units, Armor, and Maneuver:
                    float percentile = Random.RandomFloat();

                    if (percentile > 0.75f)
                    {
                        if (currentArmorForThisAge < armorNames.Count-1)
                        {
                            
                            localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_name: " + '"'
                                + MaterialLevels[currentMaterialLevel] + " " 
                                + armorNames[currentArmorForThisAge].name + '"');

                            localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_desc: " + '"'
                                + "I know, let us hammer this " + MaterialLevels[currentMaterialLevel]
                                + " material into the shape of a " + armorNames[currentArmorForThisAge].name + '"');
                            localizationStream.Flush();
                            currentArmorForThisAge++;

                           // weaponsTechLevelCheck[currentMaterialLevel].Clear();
                            weaponsTechLevelCheck[currentMaterialLevel] = new List<Weapon>(weaponNames);
                        }
                        else
                        {
                            GenerateMilTechEntry(milTechFile);
                        }
                    }
                    else if (percentile > 0.70f)
                    {
                        if (currentManeuver < maxManeuver)
                        {
                            maxManeuver++;
                        }
                        else
                        {
                            GenerateMilTechEntry(milTechFile);
                        }
                    }
                    else
                    {
                        if (weaponsTechLevelCheck[currentMaterialLevel].Count > 0)
                        {
                            int ran = Random.RandomInt(0, weaponsTechLevelCheck[currentMaterialLevel].Count);
                            string unitName = EnableNewUnit(ran);
                            milTechFile.WriteLine("     enable = " + unitName);
                            milTechFile.Flush();

                            localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_name: " + '"'
                                + unitName.Replace("_", " "));
                            localizationStream.Flush();
                        }
                        else
                        {
                            GenerateMilTechEntry(milTechFile);
                        }
                    }

                    break;
                    #endregion 

                case 2:
                    //sergeants
                    #region sergeants tech
                    milTechFile.WriteLine("     land_morale = 0.01");

                    string sergeantsDescription; 

                    switch (Random.RandomInt(1, 7))
                    {
                        case 1: sergeantsDescription = "I don't know but I've been told..."; break;
                        case 2: sergeantsDescription = "Pointy end in the enemy. It's not hard people"; break;
                        case 3: sergeantsDescription = "I HAVE NEVER SEEN SUCH A DISGRACE IN THIS HERE ARMY BEFORE IN MY LIFE"; break;
                        case 4: sergeantsDescription = "See that? That is how NOT to do it"; break;
                        case 5: sergeantsDescription = "I have no words for how bad that was"; break;
                        default: sergeantsDescription = "They shout at the troops. The troops find more morale"; break;
                    }

                    localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_name: " + '"'
                                + "Sergeants " + ToRoman(currentSergeantsTech) + '"');

                    localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_desc: " + '"'
                        + sergeantsDescription + '"');
                    localizationStream.Flush();

                    currentSergeantsTech++;
                    break;
                    #endregion

                case 3:
                    //sergeants
                    #region shock and awe tech
                    milTechFile.WriteLine("     infantry_shock = 0.01");
                    milTechFile.WriteLine("     cavalry_shock = 0.01");
                    milTechFile.WriteLine("     artillery_shock = 0.01");

                    string shockdesc;

                    switch (Random.RandomInt(1, 7))
                    {
                        case 1: shockdesc = "CHARGE!"; break;
                        case 2: shockdesc = "Strike shiftly and suddenly"; break;
                        case 3: shockdesc = "Honor demands we strike fast"; break;
                        case 4: shockdesc = "Death to our enemies"; break;
                        case 5: shockdesc = "Victory is within our grasp, we just need to take it"; break;
                        default: shockdesc = "Shock and awe win the day"; break;
                    }

                    localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_name: " + '"'
                                + "Shock and Awe " + ToRoman(currentShockTech) + '"');

                    localizationStream.WriteLine(" mil_tech_" + currentTechNo + "_desc: " + '"'
                        + shockdesc + '"');
                    localizationStream.Flush();

                    currentShockTech++;
                    break;
                    #endregion

                default:
                    GenerateMilTechEntry(milTechFile);
                    break;
            }
        }

        protected string EnableNewUnit(int weapon)
        {
            //get its name. 
            string name = (MaterialLevels[currentMaterialLevel] + "_" 
                 + weaponsTechLevelCheck[currentMaterialLevel][weapon].name + "_"
                + armorNames[currentArmorForThisAge].name + "_" 
                + trainingLevel[currentTrainingLevel].name + "_speed" + currentManeuver);
            Console.WriteLine("Creating new unit: " + name);
            
            //generate its new file. 
            GenerateNewUnitFile(name, 1,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].offensiveMorale 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].offensiveMorale 
                        + trainingLevel[currentTrainingLevel].offensiveMorale,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].defensiveMorale 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].defensiveMorale 
                        + trainingLevel[currentTrainingLevel].defensiveMorale,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].offensiveFire 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].offensiveFire 
                        + trainingLevel[currentTrainingLevel].offensiveFire,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].defensiveFire 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].defensiveFire 
                        + trainingLevel[currentTrainingLevel].defensiveFire,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].offensiveShock 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].offensiveShock 
                        + trainingLevel[currentTrainingLevel].offensiveShock,
                  weaponsTechLevelCheck[currentMaterialLevel][weapon].defensiveShock 
                    + currentMaterialLevel + armorNames[currentArmorForThisAge].defensiveShock 
                        + trainingLevel[currentTrainingLevel].defensiveShock,
                  currentManeuver);
            //add it to the list of allowed units. 
            Units[currentMaterialLevel].Add(name);

            //create the localization. 
            unitLocalizationStream.WriteLine(" " + name + ": " + '"' + MaterialLevels[currentMaterialLevel]
                + " " + weaponsTechLevelCheck[currentMaterialLevel][weapon].name + " "
                + armorNames[currentArmorForThisAge].name + " "
                + trainingLevel[currentTrainingLevel].name + '"');

            unitLocalizationStream.WriteLine(" " + name + "DESCR: " + '"' + "Using "
                + MaterialLevels[currentMaterialLevel] + " " + weaponsTechLevelCheck[currentMaterialLevel][weapon].name
                + " and wearing " + armorNames[currentArmorForThisAge].name + ", these men are trained as " + trainingLevel[currentTrainingLevel].name + '"');

            unitLocalizationStream.Flush();
            weaponsTechLevelCheck[currentMaterialLevel].RemoveAt(weapon); //removes so cannot discover again.
            return name;
        }

        protected void GenerateNewUnitFile(string name, int type, int offMor, int defmor, 
            int offfire, int defire, int offshock, int defshock, int manouver)
        {
            StreamWriter newUnitStream = File.CreateText(Constants.UNITS + "/" + name + ".txt");
            newUnitStream.WriteLine("# " + name);
            newUnitStream.WriteLine();

           /* foreach (string techgroup in techGroups)
            {
                newUnitStream.WriteLine("unit_type = " + techgroup);
                newUnitStream.Flush();
            }*/


            switch (type)
            {
                case 1: newUnitStream.WriteLine("type = infantry"); break;
                case 2: newUnitStream.WriteLine("type = cavalry"); break;
                case 3: newUnitStream.WriteLine("type = artillery"); break;
            }
            newUnitStream.Flush();

            newUnitStream.WriteLine("maneuver = " + manouver);
            newUnitStream.WriteLine("offensive_morale = " + offMor);
            newUnitStream.WriteLine("defensive_morale = " + defmor);
            newUnitStream.WriteLine("offensive_fire = " + offfire);
            newUnitStream.WriteLine("defensive_fire = " + defire);
            newUnitStream.WriteLine("offensive_shock = " + offshock);
            newUnitStream.WriteLine("defensive_shock = " + defshock);

            newUnitStream.Close();
        }


        protected void GenerateAdmTech()
        {
            StreamWriter admtechFile = File.CreateText(Constants.COMMON + "/technologies/adm.txt");

            List<List<string>> governmentTypes = new List<List<string>>();

            for (int idx = 0; idx < MaterialLevels.Count; idx++)
            {
                governmentTypes.Add(new List<string>());
            }

            governmentTypes[0].Add("tribal_despotism");
            governmentTypes[0].Add("tribal_federation");
            governmentTypes[0].Add("tribal_democracy");

            governmentTypes[1].Add("theocratic_government");

            governmentTypes[2].Add("despotic_monarchy");
            governmentTypes[2].Add("feudal_monarchy");
            governmentTypes[2].Add("republican_dictatorship");

            governmentTypes[3].Add("noble_republic");

            governmentTypes[4].Add("administrative_monarchy");
            governmentTypes[4].Add("administrative_republic");

            governmentTypes[5].Add("absolute_monarchy");

            governmentTypes[6].Add("constitutional_monarchy");
            governmentTypes[6].Add("constitutional_republic");
            governmentTypes[6].Add("enlightened_despotism");
            governmentTypes[6].Add("bureaucratic_despotism");
            governmentTypes[6].Add("revolutionary_republic");
            governmentTypes[6].Add("revolutionary_empire");

            //create list of buildings. 
            buildingNames.Add("fort1");
            buildingNames.Add("temple");
            buildingNames.Add("marketplace");
            buildingNames.Add("armory");
            buildingNames.Add("constable");
            buildingNames.Add("dock");
            buildingNames.Add("glorious_monument");
            buildingNames.Add("training_fields");
            buildingNames.Add("farm_estate");
            buildingNames.Add("embassy");
            buildingNames.Add("fort2");
            buildingNames.Add("workshop");
            buildingNames.Add("trade_depot");
            buildingNames.Add("weapons");
            buildingNames.Add("courthouse");
            buildingNames.Add("drydock");
            buildingNames.Add("fort3");
            buildingNames.Add("counting_house");
            buildingNames.Add("canal");
            buildingNames.Add("barracks");
            buildingNames.Add("spy_agency");
            buildingNames.Add("shipyard");
            buildingNames.Add("plantations");
            buildingNames.Add("wharf");
            buildingNames.Add("treasury_office");
            buildingNames.Add("tradecompany");
            buildingNames.Add("regimental_camp");
            buildingNames.Add("town_hall");
            buildingNames.Add("grand_shipyard");
            buildingNames.Add("fort4");
            buildingNames.Add("textile");
            buildingNames.Add("road_network");
            buildingNames.Add("arsenal");
            buildingNames.Add("college");
            buildingNames.Add("post_office");
            buildingNames.Add("fort5");
            buildingNames.Add("mint");
            buildingNames.Add("naval_arsenal");
            buildingNames.Add("conscription_center");
            buildingNames.Add("university");
            buildingNames.Add("customs_house");
            buildingNames.Add("cathedral");
            buildingNames.Add("naval_base");
            buildingNames.Add("fort6");
            buildingNames.Add("stock_exchange");
            buildingNames.Add("admiralty");
            buildingNames.Add("refinery");
            buildingNames.Add("grain_depot");
            buildingNames.Add("royal_palace");
            buildingNames.Add("tax_assessor");
            buildingNames.Add("war_college");
            

            //reset variables. 
            currentTechNo = 0;
            currentYear = Constants.STARTDATE;
            currentMaterialLevel = 0;

             //write out base tech level 
            admtechFile.WriteLine("monarch_power = ADM");
            admtechFile.WriteLine();
            admtechFile.WriteLine();
            admtechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
            admtechFile.WriteLine("technology = {");
            admtechFile.WriteLine("     # tech " + currentTechNo);
            admtechFile.WriteLine("     year = " + currentYear);
            admtechFile.WriteLine("     tribal_despotism = yes");
            admtechFile.WriteLine("     tribal_federation = yes");
            admtechFile.WriteLine("     tribal_democracy = yes");
            currentTechNo++;

            admtechFile.WriteLine("}");
            admtechFile.Flush();

            int techsToNextEpoch = 10;
            int allowedIdeaGroups = 0;

            while (currentYear < Constants.ENDDATE)
            {
                currentYear += Random.RandomInt(MINYEARSBETWEENTECHS, MAXYEARSBETWEENTECHS);
                if (techsToNextEpoch < 0 && currentMaterialLevel < MaterialLevels.Count-1)
                {
                        currentMaterialLevel++;
                        allowedIdeaGroups++;
                        techsToNextEpoch = Random.RandomInt(20, 100);
                        techsToNextEpoch *= currentMaterialLevel;
                        admtechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
                        admtechFile.WriteLine("technology = {");
                        admtechFile.WriteLine("     # tech " + currentTechNo);
                        admtechFile.WriteLine("     year = " + currentYear);

                        foreach (string government in governmentTypes[currentMaterialLevel])
                        {
                            admtechFile.WriteLine("     " + government + " = yes");
                        }
                        admtechFile.WriteLine("     allowed_idea_groups = " + allowedIdeaGroups);

                        admtechFile.WriteLine("}");
                        admtechFile.Flush();
                        localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                    + MaterialLevels[currentMaterialLevel] + " age" + '"');

                        localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                            + "Now we have advanced to the " + MaterialLevels[currentMaterialLevel] + " age" + '"');
                        localizationStream.Flush();
                    
                }
                else
                {
                    admtechFile.WriteLine("technology = {");
                    admtechFile.WriteLine("     # tech " + currentTechNo);
                    admtechFile.WriteLine("     year = " + currentYear);
                    admtechFile.Flush();
                    GenerateAdmTechEntry(admtechFile);
                    admtechFile.WriteLine("}");
                }

                
                currentTechNo++;
                techsToNextEpoch--;
            }

            admtechFile.Flush();
            admtechFile.Close();

        }

        protected void GenerateAdmTechEntry(StreamWriter admTechFile)
        {
            
            switch(Random.RandomInt(1, 5))
            {
                case 1:
                    #region production tech
                    admTechFile.WriteLine("     production_efficiency = 0.01");

                    string productionDescription; 

                    switch (Random.RandomInt(1, 7))
                    {
                        case 1: productionDescription = "My new assitant has figured out this clever way of sorting all my tools"; break;
                        case 2: productionDescription = "Right, kill one of them, the rest will find much more motivation"; break;
                        case 3: productionDescription = "Eureka! This will help so much"; break;
                        case 4: productionDescription = "So this gear will help?"; break;
                        case 5: productionDescription = "Standardization makes this much better"; break;
                        default: productionDescription = "The Workshops of the land have been improved"; break;
                    }

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                + "Workshops " + ToRoman(currentProductionTech) + '"');

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                        + productionDescription + '"');
                    localizationStream.Flush();

                    currentProductionTech++;
                    break;
                    #endregion

                case 2: 
                    #region Roads tech
                    admTechFile.WriteLine("     maneuver_value = 0.01");

                    string roadsdesc; 

                    switch (Random.RandomInt(1, 4))
                    {
                        case 1: roadsdesc = "New Techniques in paving roads"; break;
                        default: roadsdesc = "Better roads increase the speed at which you can travel"; break;
                    }

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                + "Roads " + ToRoman(currentRoadsTech) + '"');

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                        + roadsdesc + '"');
                    localizationStream.Flush();

                    currentRoadsTech++;
                    break;
                    #endregion

                case 3:
                    #region supply tech
                    admTechFile.WriteLine("     supply_limit = 0.01");

                    string supplyLinesDesc; 

                    switch (Random.RandomInt(1, 7))
                    {
                        case 1: supplyLinesDesc = "The Army requires food, so fork it over"; break;
                        case 2: supplyLinesDesc = "We'll just take this food instead of taxes"; break;
                        case 3: supplyLinesDesc = "What a clever new supply plan. Watch him will you, make sure he is not after our jobs"; break;
                        default: supplyLinesDesc = "New improvements to supply policies will help us support more troops"; break;
                    }

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                + "Workshops " + ToRoman(currentSupplyTech) + '"');

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                        + supplyLinesDesc + '"');
                    localizationStream.Flush();

                    currentSupplyTech++;
                    break;
                    #endregion

                case 4: 
                    #region supply tech

                    if (currentBuildings < buildingNames.Count)
                    {
                        admTechFile.WriteLine("     " + buildingNames[currentBuildings] + " = yes");

                        string constructiondesc;

                        switch (Random.RandomInt(1, 7))
                        {
                            case 1: constructiondesc = "Better methods for construction"; break;
                            case 2: constructiondesc = "Hmmm, this is a much better material to make that building"; break;
                            case 3: constructiondesc = "What a nice building"; break;
                            default: constructiondesc = "Advances in construction techniques allow for more purposes for buildings"; break;
                        }

                        localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                    + "Construction " + ToRoman(currentSupplyTech) + '"');

                        localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                            + constructiondesc + '"');
                        localizationStream.Flush();

                        currentBuildings++;
                    }
                    else
                    {
                        GenerateAdmTechEntry(admTechFile);
                    }
                    break;
                    #endregion

                default: GenerateAdmTechEntry(admTechFile); break;
            }
        }

        protected void GenerateDipTech()
        {
            StreamWriter dipTechFile = File.CreateText(Constants.COMMON + "/technologies/dip.txt");

            

            //reset variables. 
            currentTechNo = 0;
            currentYear = Constants.STARTDATE;
            currentMaterialLevel = 0;

            //write out base tech level 
            dipTechFile.WriteLine("monarch_power = DIP");
            dipTechFile.WriteLine();
            dipTechFile.WriteLine();
            dipTechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
            dipTechFile.WriteLine("technology = {");
            dipTechFile.WriteLine("     # tech " + currentTechNo);
            dipTechFile.WriteLine("     year = " + currentYear);
            dipTechFile.WriteLine("     enable = cog"); //note, placeholder. 
            dipTechFile.WriteLine("     enable = galley");
            dipTechFile.WriteLine("     enable = barque");
            dipTechFile.WriteLine("     enable = early_carrack");
            dipTechFile.WriteLine("     merchants = yes");
            currentTechNo++;

            dipTechFile.WriteLine("}");
            dipTechFile.Flush();

            int techsToNextEpoch = 10;

            while (currentYear < Constants.ENDDATE)
            {
                currentYear += Random.RandomInt(MINYEARSBETWEENTECHS, MAXYEARSBETWEENTECHS);
                if (techsToNextEpoch < 0 && currentMaterialLevel < MaterialLevels.Count-1)
                {
                    currentMaterialLevel++;
                    techsToNextEpoch = Random.RandomInt(20, 100);
                    techsToNextEpoch *= currentMaterialLevel;
                    dipTechFile.WriteLine("# " + MaterialLevels[currentMaterialLevel] + " age techs");
                    dipTechFile.WriteLine("technology = {");
                    dipTechFile.WriteLine("     # tech " + currentTechNo);
                    dipTechFile.WriteLine("     year = " + currentYear);
                    dipTechFile.WriteLine("}");
                    dipTechFile.Flush();
                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_name: " + '"'
                                + MaterialLevels[currentMaterialLevel] + " age" + '"');

                    localizationStream.WriteLine(" adm_tech_" + currentTechNo + "_desc: " + '"'
                        + "Now we have advanced to the " + MaterialLevels[currentMaterialLevel] + " age" + '"');
                    localizationStream.Flush();

                }
                else
                {
                    dipTechFile.WriteLine("technology = {");
                    dipTechFile.WriteLine("     # tech " + currentTechNo);
                    dipTechFile.WriteLine("     year = " + currentYear);
                    dipTechFile.Flush();
                    GenerateDipTechEntry(dipTechFile);
                    dipTechFile.WriteLine("}");
                }


                currentTechNo++;
                techsToNextEpoch--;
            }

            dipTechFile.Flush();
            dipTechFile.Close();
        }
        protected void GenerateDipTechEntry(StreamWriter diptechFile)
        {
            switch(Random.RandomInt(1, 3))
            {
                case 1: 
                    #region Trade tech
                    diptechFile.WriteLine("     trade_range = 0.1");

                    string tradedesc; 

                    switch (Random.RandomInt(1, 4))
                    {
                        case 1: tradedesc = "New Techniques in Trading"; break;
                        default: tradedesc = "Increase the range at which we can trade"; break;
                    }

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_name: " + '"'
                                + "Trading " + ToRoman(currentTradeTech) + '"');

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_desc: " + '"'
                        + tradedesc + '"');
                    localizationStream.Flush();

                    currentTradeTech++;
                    break;
                    #endregion

                case 2: 
                    #region Range tech
                    diptechFile.WriteLine("     trade_range = 1");

                    string rangedesc; 

                    switch (Random.RandomInt(1, 4))
                    {
                        case 1: rangedesc = "Perhaps just over the next hill?"; break;
                        default: rangedesc = "Increase the range at which we settle"; break;
                    }

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_name: " + '"'
                                + "Settling " + ToRoman(currentRangeTech) + '"');

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_desc: " + '"'
                        + rangedesc + '"');
                    localizationStream.Flush();

                    currentRangeTech++;
                    break;
                    #endregion

                case 3:
                    #region Naval tech
                    diptechFile.WriteLine("     naval_morale = 0.1");

                    string navalDesc; 

                    switch (Random.RandomInt(1, 4))
                    {
                        case 1: navalDesc = "Set sail for Victory"; break;
                        default: navalDesc = "Increasing the technology of our navy"; break;
                    }

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_name: " + '"'
                                + "Naval " + ToRoman(currentNavalTech) + '"');

                    localizationStream.WriteLine(" dip_tech_" + currentTechNo + "_desc: " + '"'
                        + navalDesc + '"');
                    localizationStream.Flush();

                    currentNavalTech++;
                    break;
                    #endregion



                default: 
                    GenerateDipTechEntry(diptechFile);
                    break;
            }


        }

        /// <summary>
        /// converts in to a Roman numeral string. 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToRoman(int number)
        {
            //from http://stackoverflow.com/questions/7040289/converting-integers-to-roman-numerals
            if ((number < 0) || (number > 3999)) return number.ToString();//throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }
    }

    class Weapon
    {
        public string name { get; private set; } 
        int offMor, defmor, offire, defire, offshock, defshock;
        int materialLevel;

        public int offensiveMorale { get { return offMor; /*+ materialLevel;*/ } }
        public int defensiveMorale { get { return defmor; } }
        public int offensiveFire { get { return offire; } }
        public int defensiveFire { get { return defire; } }
        public int offensiveShock { get { return offshock; } }
        public int defensiveShock { get { return defshock; } }

        public Weapon(string name, int offMor, int defmor,
            int offfire, int defire, int offshock, int defshock)
        {
            this.name = name;
            this.offMor = offMor;
            this.offire = offfire;
            this.offshock = offshock;
            this.defire = defire;
            this.defmor = defmor;
            this.defshock = defshock;
        }

        public void SetMaterialLevel(int materialLevel)
        {
            this.materialLevel = materialLevel;
        }

    }
}
