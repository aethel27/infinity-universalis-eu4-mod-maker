﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class TagGeneration
    {
        List<Tag> tags = new List<Tag>();

        public void GenerateTag()
        {
            Tag newTag = new Tag(TagString());
            newTag.capitalProvince = AssignCapitalToTag(); 
            tags.Add(newTag);
            
        }

        int AssignCapitalToTag()
        {
            int capital = CapitalAssignment.AssignCapital();

            for (int idx = 0; idx < tags.Count; idx++)
            {
                if (tags[idx].capitalProvince == capital)
                {
                    capital = AssignCapitalToTag();
                    idx = tags.Count;
                }
            }
            return capital;
        }

        string TagString()
        {
            char[] tagId = { returnCharacter(), returnCharacter(), returnCharacter() };
            //char[] tagId = { Convert.ToChar(Random.RandomInt(48, 57)), returnCharacter(), returnCharacter() };
            string tagstring = new string(tagId);
            for (int idx = 0; idx < tags.Count; idx++)
            {
                if (tags[idx].Id == tagstring || tagstring == "REB" || tagstring == "PIR" || tagstring == "NAT")
                {
                    tagstring = TagString();
                    idx = tags.Count; 
                }
            }
            return tagstring;
        }

        char returnCharacter()
        {
            int num = Random.RandomInt(0, 26); // Zero to 25
            char let = (char)('A' + num);
            return let;
        }

        public void WriteTags()
        {
            System.IO.Directory.CreateDirectory(Constants.COMMON + "/country_tags");
            Console.WriteLine("Country tags directory created");
            StreamWriter countries;
            countries = File.CreateText(Constants.COMMON + "/country_tags/00_countries.txt");
           /* //write the special countries. 
            countries.WriteLine("#Special Countries");
            countries.WriteLine("REB = " + '"' + "countries/Rebels.txt" + '"');
            countries.WriteLine("PIR = " + '"' + "countries/Pirates.txt"+'"');
            countries.WriteLine("NAT = " + '"' + "countries/Natives.txt" +'"');

            */

            //pull tags from the old list, as there is too many files they are linked in to replace for now. 
            StreamReader oldCountries = new StreamReader(Constants.INSTALLPATH + "/common/country_tags/00_countries.txt");
            string line;
            while ((line = oldCountries.ReadLine()) != null)
            {
                countries.WriteLine(line);
                countries.Flush();
            }

            countries.WriteLine("#Generated Countries");


            int count = 0;
            foreach (Tag tag in tags)
            {
                countries.WriteLine(tag.Id + " = " + '"' + "countries/" + tag.countryName + ".txt" + '"');
                countries.Flush();
                count++;
            }
            countries.Close();
            Console.WriteLine(count + " lines written");
            Console.WriteLine("00_countries file written");
        }

        public void WriteCountryColors()
        {
            System.IO.Directory.CreateDirectory(Constants.COMMON + "/country_colors");
            Console.WriteLine("Country tags directory created");
            StreamWriter countries;
            countries = File.CreateText(Constants.COMMON + "/country_colors/00_country_colors.txt");

            foreach (Tag tag in tags)
            {
                tag.WriteUnitColors(countries);
            }
            countries.Close();
            Console.WriteLine("Country Colors written");
        }

        /// <summary>
        /// Writes the indivdual country files and creates the required directory. 
        /// </summary>
        public void WriteCountries(List<List<string>> UnitLists)
        {
            

            //make sure that all the files work properly by erasing the old files.  
            string[] countryFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/common/countries");
            foreach (string fileName in countryFileNames)
            {
                string filenameWithoutPath = Path.GetFileName(fileName);
                File.CreateText(Constants.COMMON + "/countries/" + filenameWithoutPath);
            }

            Console.WriteLine("Countries directory created");

            Tag reb = new Tag("REB", "Rebels", 30, 30, 30);
            reb.GenerateAndWriteCountryFile(UnitLists);
            Tag pir = new Tag("PIR", "Prirates", 30, 30, 30);
            pir.GenerateAndWriteCountryFile(UnitLists);
            Tag nat = new Tag("NAT", "Natives", 30, 30, 30);
            nat.GenerateAndWriteCountryFile(UnitLists);

            foreach (Tag tag in tags)
            {
                tag.GenerateAndWriteCountryFile(UnitLists);
            }
            Console.WriteLine("Country files written");
        }

        public void WriteHistoryFiles(Technology tech)
        {

            string[] countryFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/history/countries");
            foreach (string fileName in countryFileNames)
            {
                string filenameWithoutPath = Path.GetFileName(fileName);
                File.CreateText(Constants.HISTORY + "/countries/" + filenameWithoutPath);
            }


            try
            {
                StreamWriter file;
                file = new StreamWriter(Constants.HISTORY + "/countries/REB - Rebels.txt");
                Console.WriteLine("writing history file for Rebels");
                file.WriteLine("technology_group = western");
                file.Close();
                file = new StreamWriter(Constants.HISTORY + "/countries/PIR - Pirates.txt");
                Console.WriteLine("writing history file for Pirates");
                file.WriteLine("technology_group = western");
                file.Close();
                file = new StreamWriter(Constants.HISTORY + "/countries/NAT - Natives.txt");
                Console.WriteLine("writing history file for Natives");
                file.WriteLine("technology_group = western");
                file.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("ERROR: " + e.Message);
            }


            foreach (Tag tag in tags)
            {
                tag.GenerateHistoryFile(tech.techGroups[Random.RandomInt(0, tech.techGroups.Count)]);
            }

            Console.WriteLine("Country's History Files written");

        }

        public void WriteCultureFile()
        {
            System.IO.Directory.CreateDirectory(Constants.COMMON + "/cultures");
            Console.WriteLine("Country tags directory created");
            StreamWriter cultures;
            cultures = File.CreateText(Constants.COMMON + "/cultures/00_cultures.txt");
            /* //write the special countries. 
             countries.WriteLine("#Special Countries");
             countries.WriteLine("REB = " + '"' + "countries/Rebels.txt" + '"');
             countries.WriteLine("PIR = " + '"' + "countries/Pirates.txt"+'"');
             countries.WriteLine("NAT = " + '"' + "countries/Natives.txt" +'"');

             */

            //pull tags from the old list, as there is too many files they are linked in to replace for now. 
            StreamReader oldCountries = new StreamReader(Constants.INSTALLPATH + "/common/cultures/00_cultures.txt");
            string line;
            while ((line = oldCountries.ReadLine()) != null)
            {
                cultures.WriteLine(line);
                cultures.Flush();
            }

            foreach (Tag tag in tags)
            {
                cultures.WriteLine(tag.countryCulture + "_group = {");
                cultures.WriteLine("    graphical_culture = " + tag.graphicalCulture);
                cultures.WriteLine("    " + tag.countryCulture + " = {");
                cultures.WriteLine("        primary = " + tag.Id);
                cultures.WriteLine("    }");
                cultures.WriteLine("    dynasty_names = {");
                cultures.WriteLine();

                //create random dynasty names. 
                int randomDynasties = Random.RandomInt(3, 20);
                for (int idx = 0; idx < randomDynasties; idx++)
                {
                    cultures.Write(" " + NameGenerator.GenerateName() + " ");
                }
                cultures.WriteLine("    }");
                cultures.WriteLine("}");
                cultures.Flush();
            }
            cultures.Close();
            Console.WriteLine("Cultures file created");
        }

        public void WriteProvinceHistory()
        {
            System.IO.Directory.CreateDirectory(Constants.HISTORY + "/provinces");



            //for (int idx = 1; idx < 2003; idx++)
            //make sure that all the files work properly by erasing the old files.  
            string[] provinceFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/history/provinces");
            /*foreach (string fileName in provinceFileNames)
            {
                string filenameWithoutPath = Path.GetFileName(fileName);
                File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath);
            }*/


            foreach (string fileName in provinceFileNames)
            {
                string filenameWithoutPath = Path.GetFileName(fileName);

                StreamWriter file;
                file = File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath);
                Console.WriteLine("creating province " + filenameWithoutPath + "'s history file");
                file = File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath + ".txt");
                int idxLength;
                if (filenameWithoutPath.Contains("-"))
                {
                    idxLength = filenameWithoutPath.IndexOf("-");
                }
                else
                {
                    idxLength = filenameWithoutPath.IndexOf(" ");
                }
                int idx =  Convert.ToInt32(filenameWithoutPath.Substring(0, idxLength));

                if (!CapitalAssignment.forbidden(idx))
                {
                    for (int jdx = 0; jdx < tags.Count; jdx++)
                    {
                        if (idx == tags[jdx].capitalProvince)
                        {
                            file.WriteLine("add_core = " + tags[jdx].Id);
                            file.WriteLine("owner = " + tags[jdx].Id);
                            file.WriteLine("controller = " + tags[jdx].Id);
                            //file.WriteLine("1444.1.1 = { owner = " + tags[jdx].Id
                            //  + " controller = " + tags[jdx].Id + "}");
                            file.WriteLine("culture = " + tags[jdx].countryCulture);
                            //file.WriteLine("religion = catholic");
                            file.WriteLine("fort1 = yes");
                            file.WriteLine("is_city = yes");
                            file.WriteLine("base_tax = 20");

                            jdx = tags.Count;
                        }
                        else if (jdx == tags.Count - 1)
                        {
                            //remove other nations from the map. 
                            //file.WriteLine("owner = XX1");
                            //file.WriteLine("controller = XX1");
                            //file.WriteLine("1444.1.1  = { owner = 111 controller = 111 }");
                            file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                            //file.WriteLine("culture = gascon");
                        }
                    }

                    file.WriteLine("religion = animism");

                    file.WriteLine("capital = " + NameGenerator.GenerateName());
                    file.WriteLine("hre = no");
                    //file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                    file.WriteLine("manpower = " + Random.RandomInt(1, 10));
                    file.WriteLine("tradegoods = unknown");
                    //file.WriteLine("discovered_by = western");
                }
                else
                {
                    // file.WriteLine("discovered_by = western");
                }
                file.Flush();
                file.Close();
            }
            Console.WriteLine("finished creating Province history files");
        }

        public void WriteProvinceHistory(Map thisMap)
        {
            System.IO.Directory.CreateDirectory(Constants.HISTORY + "/provinces");

            

            //for (int idx = 1; idx < 2003; idx++)
            //make sure that all the files work properly by erasing the old files.  
            string[] provinceFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/history/provinces");
            foreach (string fileName in provinceFileNames)
            {
                string filenameWithoutPath = Path.GetFileName(fileName);
                File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath);
            }


            foreach (Province province in thisMap.provinceList)//(string fileName in provinceFileNames)
            {
                //string filenameWithoutPath = Path.GetFileName(fileName);

                StreamWriter file;
                //file = File.CreateText(Constants.HISTORY + "/provinces/" + filenameWithoutPath);
                Console.WriteLine("creating province " + province.provinceNo + "'s history file");
                file = File.CreateText(Constants.HISTORY + "/provinces/" + province.provinceNo + ".txt");
                /*int idxLength;
                if (filenameWithoutPath.Contains("-"))
                {
                    idxLength = filenameWithoutPath.IndexOf("-");
                }
                else
                {
                    idxLength = filenameWithoutPath.IndexOf(" ");
                }*/
                int idx = province.provinceNo; // Convert.ToInt32(filenameWithoutPath.Substring(0, idxLength));

                if (!CapitalAssignment.forbidden(idx))
                {
                    for (int jdx = 0; jdx < tags.Count; jdx++)
                    {
                        if (idx == tags[jdx].capitalProvince)
                        {
                            file.WriteLine("add_core = " + tags[jdx].Id);
                            file.WriteLine("owner = " + tags[jdx].Id);
                            file.WriteLine("controller = " + tags[jdx].Id);
                            //file.WriteLine("1444.1.1 = { owner = " + tags[jdx].Id
                              //  + " controller = " + tags[jdx].Id + "}");
                            file.WriteLine("culture = " + tags[jdx].countryCulture);
                            //file.WriteLine("religion = catholic");
                            file.WriteLine("fort1 = yes");
                            file.WriteLine("is_city = yes");
                            file.WriteLine("base_tax = 20");

                            jdx = tags.Count;
                        }
                        else if (jdx == tags.Count - 1) 
                        {
                            //remove other nations from the map. 
                            //file.WriteLine("owner = XX1");
                            //file.WriteLine("controller = XX1");
                            //file.WriteLine("1444.1.1  = { owner = 111 controller = 111 }");
                            file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                            //file.WriteLine("culture = gascon");
                        }
                    }

                    file.WriteLine("religion = animism");

                    file.WriteLine("capital = " + NameGenerator.GenerateName());
                    file.WriteLine("hre = no");
                    //file.WriteLine("base_tax = " + Random.RandomInt(1, 10));
                    file.WriteLine("manpower = " + Random.RandomInt(1, 10));
                    file.WriteLine("tradegoods = unknown");
                    //file.WriteLine("discovered_by = western");
                }
                else
                {
                   // file.WriteLine("discovered_by = western");
                }
                file.Flush();
                file.Close();
            }
            Console.WriteLine("finished creating Province history files");
        }

        public void CreateFlags()
        {
            
            System.IO.Directory.CreateDirectory(Constants.GFX + "/flags");

            Flags flagGeneration = new Flags();

            foreach (Tag tag in tags)
            {
                Console.WriteLine("Generating flag for " + tag.Id);
                try
                {
                    flagGeneration.GenerateFlag(tag.Id);
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine(e.FusionLog);

                    Console.ReadKey();
                }
            }
            Console.WriteLine("Flags Written");
        }

        public void WriteTagLocalization()
        {
            StreamWriter englishFile;
            englishFile = File.CreateText(Constants.LOCAL + "/countries_l_english.yml");

            englishFile.WriteLine("l_english:");
            Console.WriteLine("Writing english country localizations");

            //add in default tags
            englishFile.WriteLine(" REB: " + '"' + "Rebels" + '"');
            englishFile.WriteLine(" PIR: " + '"' + "Pirates" + '"');
            englishFile.WriteLine(" NAT: " + '"' + "Natives" + '"');

            foreach (Tag tag in tags)
            {
                string correctedName = tag.countryName;
                correctedName = correctedName.Replace("_", " "); //need to look into. 
                englishFile.WriteLine(" " + tag.Id + ": " + '"' + correctedName + '"');
                englishFile.WriteLine(" " + tag.Id + "_ADJ: " + '"' + correctedName + '"');
                englishFile.Flush();
            }
            englishFile.Close();
            Console.WriteLine("English country localizations written");
        }

        public void WriteProvinceLocalization()
        {
            StreamWriter englishFile;
            englishFile = File.CreateText(Constants.LOCAL + "/prov_names_l_english.yml");

            englishFile.WriteLine("l_english:");
            Console.WriteLine("Writing english province localizations");

            for (int idx = 0; idx < 2003; idx++)
            {
                bool named = false;
                for (int jdx = 0; jdx < tags.Count; jdx++) 
                {
                    if (tags[jdx].capitalProvince == idx)
                    {
                        float percentile = Random.RandomFloat();
                        string provinceName = tags[jdx].countryName;
                        
                        if (provinceName.Contains("_"))
                        {
                            provinceName = provinceName.Substring(0, provinceName.IndexOf("_"));
                        }

                        if (percentile < 0.10f)
                        {
                            provinceName += "ia";
                        }
                        else if (percentile < 0.20f && percentile >= 0.10f)
                        {
                            provinceName += "burg";
                        }
                        else if (percentile >= 0.20f && percentile < 0.50f)
                        {
                            provinceName += "land";
                        }
                        else if (percentile >= 0.50f && percentile < 0.90f)
                        {
                            provinceName = NameGenerator.GenerateName();
                        }

                        englishFile.WriteLine(" PROV" + idx + ": " + '"' + provinceName + '"');
                        named = true;
                        jdx = tags.Count;
                    }
                }
                if (!named)
                {
                    englishFile.WriteLine(" PROV" + idx + ": " + '"' + NameGenerator.GenerateName() + '"');
                }
                englishFile.Flush();
            }
            englishFile.Close();
        }
    }
}

