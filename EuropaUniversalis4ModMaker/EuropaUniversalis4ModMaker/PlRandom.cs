﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EuropaUniversalis4ModMaker
{
    static class Random
    {
        private static System.Random random;

        /// <summary>
        /// Initializes the random with a random seed. 
        /// </summary>
        public static void Init()
        {
            random = new System.Random();
        }
        /// <summary>
        /// Sets the seed as a specific value. 
        /// </summary>
        /// <param name="seed">Seed the random numbers will be based off</param>
        public static void Init(int seed)
        {
            random = new System.Random(seed);
        }
        /// <summary>
        /// returns a random float
        /// </summary>
        /// <param name="min">minimum value</param>
        /// <param name="max">maximum value</param>
        /// <returns></returns>
        public static float RandomFloat(float min, float max)
        {
            return (float)random.NextDouble() * (max - min) + min;
        }
        /// <summary>
        /// returns a completely random float between 0 and 1
        /// </summary>
        /// <returns></returns>
        public static float RandomFloat()
        {
            return (float)random.NextDouble();
        }
        /// <summary>
        /// returns a random int between a maximum and minimum values. 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int RandomInt(int min, int max)
        {
            return random.Next(min, max);
        }
        /// <summary>
        /// returns a non-negative random int. 
        /// </summary>
        /// <returns></returns>
        public static int RandomInt()
        {
            return random.Next();
        }
    }
}
