﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace EuropaUniversalis4ModMaker
{
    class Map
    {
        int xMapSize;
        int yMapSize;
        Bitmap provinceMap;
        Bitmap heightMap;

        int provinceColorRed = 0;
        int provinceColorBlue = 0;
        int provinceColorGreen = 0; //for use with sea provinces.

        int recursionLimit = 1;
        int currentRecursion = 0;

        int seaLevel = 95;


        Color landProvinceColor = Color.White;
        Color seaProvinceColor = Color.Black;

        int maxProvinceSize = 50;

        public List<Province> provinceList { get; protected set; }


        public List<Province> seaProvinceList { get; protected set; }

        public void GenerateNewMap()
        {
            Province incrementProvinceCounter = new Province(0, 0, Color.White);

            provinceList =  new List<Province>();
            seaProvinceList =  new List<Province>();
            heightMap = new Bitmap(Constants.INSTALLPATH + "/map/heightmap.bmp");

            xMapSize = heightMap.Width;
            yMapSize = heightMap.Height;

            Console.WriteLine("Creating map");
            provinceMap = new Bitmap(xMapSize, yMapSize);
            for (int idx = 0; idx < xMapSize; idx++)
            {
                for (int jdx = 0; jdx < yMapSize; jdx++)
                {
                    if (heightMap.GetPixel(idx, jdx).R > seaLevel)
                    {
                        provinceMap.SetPixel(idx, jdx, landProvinceColor);
                    }
                    else if (heightMap.GetPixel(idx, jdx).R <= seaLevel)
                    {
                        provinceMap.SetPixel(idx, jdx, seaProvinceColor);
                    }
                }
                Console.WriteLine("blank map Row completed: " + idx);
            } 
            CreateProvinceMap();
            CreateDefaultFile();
            CreatePositions();
            CreateContinents();
           // provinceMap.Save(Constants.MAP + "/provinces.bmp");

            Console.WriteLine("Created new Map");
        }

        protected void CreateProvinceMap()
        {
            #region land
            provinceColorGreen = 255;
            provinceColorBlue = 0;
            provinceColorRed = 0;
            int totalProvinceNumber = Random.RandomInt(1500, 3000);
            Console.WriteLine(totalProvinceNumber + " Provinces to be generated");
            for (int idx = 0; idx < totalProvinceNumber; idx++)
            {
                provinceList.Add(GenerateProvince(landProvinceColor));
            }

            for (int idx = 0; idx < provinceList.Count; idx++) 
            {
                CreateProvince(provinceList[idx].xStartCoord, provinceList[idx].yStartCoord, provinceList[idx].provinceColor, landProvinceColor);
            }
            //check white spaces
            for (int idx = 0; idx < xMapSize; idx++)
            {
                Console.WriteLine("checking row " + idx + " for unclaimed territory");
                for (int jdx = 0; jdx < yMapSize; jdx++)
                {
                    if (provinceMap.GetPixel(idx, jdx).ToArgb() == landProvinceColor.ToArgb())
                    {
                        Console.WriteLine("uncliamed territory found, filling now");
                        int direction = 0;
                        /*if (idx > 0)
                        {
                            if (!(provinceMap.GetPixel(idx - 1, jdx).ToArgb() == seaProvinceColor.ToArgb()))
                            {
                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx - 1, jdx));
                                direction = -1;
                            }
                        }
                        else if (jdx > 0 && direction == 0)
                        {
                            if (!(provinceMap.GetPixel(idx, jdx - 1).ToArgb() == seaProvinceColor.ToArgb()))
                            {
                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx, jdx - 1));
                                direction = -1;
                            }
                        }
                        */

                            
                            //int direction = Random.RandomInt(0, 1);

                            if (direction == 0)
                            {
                                int rightAdvance = 1;
                                bool check = true;
                                while ((rightAdvance < 10 && (idx + rightAdvance < xMapSize)) && check)
                                {
                                    if (!(provinceMap.GetPixel(idx + rightAdvance, jdx).ToArgb() == landProvinceColor.ToArgb())
                                        && !(provinceMap.GetPixel(idx + rightAdvance, jdx).ToArgb() == seaProvinceColor.ToArgb()))
                                    {
                                        check = false;
                                        for (int ndx = rightAdvance; rightAdvance <= 0; rightAdvance--)
                                        {
                                            provinceMap.SetPixel(idx + ndx, jdx, provinceMap.GetPixel(idx + rightAdvance, jdx));
                                            
                                        }
                                    }
                                    rightAdvance++;
                                    
                                }
                                if (check)
                                {
                                    direction = 1;
                                }
                            }
                            if (direction == 1)
                            {
                                int downAdvance = 1;
                                bool check = true;
                                while ((downAdvance < 10 && (jdx + downAdvance < yMapSize)) && check)
                                {
                                    if (!(provinceMap.GetPixel(idx, jdx + downAdvance).ToArgb() == landProvinceColor.ToArgb())
                                        && !(provinceMap.GetPixel(idx, jdx + downAdvance).ToArgb() == seaProvinceColor.ToArgb()))
                                    {
                                        check = false;
                                        for (int ndx = downAdvance; downAdvance <= 0; downAdvance--)
                                        {
                                            provinceMap.SetPixel(idx, jdx + ndx, provinceMap.GetPixel(idx, jdx + downAdvance));
                                        }
                                    }
                                    
                                    if (check)
                                    {
                                       provinceList.Add(GenerateProvince(idx, jdx));
                                        CreateProvince(idx, jdx, provinceList[provinceList.Count - 1].provinceColor, landProvinceColor);
                                    }
                                    downAdvance++;
                                }
                            }
                        
                    }
                }
            }
            #endregion 

            #region sea
            provinceColorGreen = 0;
            provinceColorBlue = 255;
            provinceColorRed = 0;
            Console.WriteLine("generating sea provinces");
            int totalSeaProvinceNumber = Random.RandomInt(1500, 3000);
            Console.WriteLine(totalSeaProvinceNumber + " Provinces to be generated");
            for (int idx = 0; idx < totalSeaProvinceNumber; idx++)
            {
                seaProvinceList.Add(GenerateProvince(seaProvinceColor));
            }

            for (int idx = 0; idx < seaProvinceList.Count; idx++)
            {
                CreateProvince(seaProvinceList[idx].xStartCoord, seaProvinceList[idx].yStartCoord, seaProvinceList[idx].provinceColor, seaProvinceColor);
            }
            //check white spaces
            for (int idx = 0; idx < xMapSize; idx++)
            {
                Console.WriteLine("checking row " + idx + " for unclaimed territory");
                for (int jdx = 0; jdx < yMapSize; jdx++)
                {
                    if (provinceMap.GetPixel(idx, jdx).ToArgb() == seaProvinceColor.ToArgb())
                    {
                        Console.WriteLine("uncliamed territory found, filling now");
                        int direction = 0;
                        if (idx > 0)
                        {
                            if ((provinceMap.GetPixel(idx - 1, jdx).B == 255))
                            {
                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx - 1, jdx));
                                direction = -1;
                            }
                        }
                        else if (jdx > 0 && direction == 0)
                        {
                            if ((provinceMap.GetPixel(idx, jdx - 1).B == 255))
                            {
                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx, jdx - 1));
                                direction = -1;
                            }
                        }



                        //int direction = Random.RandomInt(0, 1);

                        if (direction == 0)
                        {
                            int rightAdvance = 1;
                            bool check = true;
                            while ((rightAdvance < 10 && (idx + rightAdvance < xMapSize)) && check)
                            {
                                if (!(provinceMap.GetPixel(idx + rightAdvance, jdx).ToArgb() == landProvinceColor.ToArgb())
                                    && !(provinceMap.GetPixel(idx + rightAdvance, jdx).ToArgb() == seaProvinceColor.ToArgb())
                                    && provinceMap.GetPixel(idx + rightAdvance, jdx).B == 255)
                                {
                                    check = false;
                                    for (int ndx = rightAdvance; rightAdvance <= 0; rightAdvance--)
                                    {
                                        provinceMap.SetPixel(idx + ndx, jdx, provinceMap.GetPixel(idx + rightAdvance, jdx));

                                    }
                                }
                                rightAdvance++;

                            }
                            if (check)
                            {
                                direction = 1;
                            }
                        }
                        if (direction == 1)
                        {
                            int downAdvance = 1;
                            bool check = true;
                            while ((downAdvance < 10 && (jdx + downAdvance < yMapSize)) && check)
                            {
                                if (!(provinceMap.GetPixel(idx, jdx + downAdvance).ToArgb() == landProvinceColor.ToArgb())
                                    && !(provinceMap.GetPixel(idx, jdx + downAdvance).ToArgb() == seaProvinceColor.ToArgb())
                                    && provinceMap.GetPixel(idx, jdx + downAdvance).B == 255)
                                {
                                    check = false;
                                    for (int ndx = downAdvance; downAdvance <= 0; downAdvance--)
                                    {
                                        provinceMap.SetPixel(idx, jdx + ndx, provinceMap.GetPixel(idx, jdx + downAdvance));
                                    }
                                }

                                if (check)
                                {
                                    seaProvinceList.Add(GenerateProvince(idx, jdx));
                                    CreateProvince(idx, jdx, provinceList[provinceList.Count - 1].provinceColor, seaProvinceColor);
                                }
                                downAdvance++;
                            }
                        }

                    }
                }
            }
            #endregion
            
            #region oldCheckCode
            /* bool containsUnclaimedLand;
            bool changed;

            int provinceHaveExpandedBy = 0;

            int looped = 0; 
            do
            {
                //loop until all white pixels are gone. 
                containsUnclaimedLand = false;
                changed = false;

                do
                {
                    //loop until provinces think they cannot expand any more. 
                    changed = false;
                    Console.WriteLine("Beginning Expansion of provinces");
                    foreach (Province province in provinceList)
                    {
                       // for (int idx = 0; idx < 10; idx++)
                        //{
                            currentRecursion = 0;
                            bool test = ExpandProvince(province.xStartCoord, province.yStartCoord, province.provinceColor);
                            if (test)
                            {
                                changed = true;
                            }
                            
                        //}
                    }
                    Console.WriteLine("provinces expanded by " + provinceHaveExpandedBy);
                    provinceHaveExpandedBy++;
                    recursionLimit++;

                } while (changed);

                Console.WriteLine("provinces think they can expand no more");
                /*
                for (int idx = 0; idx < xMapSize; idx++)
                {
                    for (int jdx = 0; jdx < yMapSize; jdx++) 
                    {
                        if (provinceMap.GetPixel(idx, jdx).ToArgb() == landProvinceColor.ToArgb())
                        {
                            //containsUnclaimedLand = true;
                            Console.WriteLine(" Map contains Unclaimed Land. Filling now");
                            //GenerateProvince(idx, jdx); //stop insane amounts of looping. 
                            
                            looped++;
                            if (looped > 1)
                            {
                                if (idx + 1 < xMapSize)
                                {
                                    if (!(provinceMap.GetPixel(idx + 1, jdx).ToArgb() == landProvinceColor.ToArgb())
                                        && !(provinceMap.GetPixel(idx + 1, jdx).ToArgb() == seaProvinceColor.ToArgb()))
                                    {
                                        provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx + 1, jdx));
                                    }
                                    else if (jdx + 1 < yMapSize)
                                    {
                                        if (!(provinceMap.GetPixel(idx, jdx + 1).ToArgb() == landProvinceColor.ToArgb())
                                            && !(provinceMap.GetPixel(idx, jdx + 1).ToArgb() == seaProvinceColor.ToArgb()))
                                        {
                                            provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx, jdx + 1));
                                        }
                                        else if ((idx - 1 > 0) && (jdx - 1 > 0))
                                        {
                                            if (provinceMap.GetPixel(idx - 1, jdx).ToArgb() == provinceMap.GetPixel(idx, jdx + 1).ToArgb())
                                            {
                                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx - 1, jdx));
                                            }
                                            else if (provinceMap.GetPixel(idx, jdx-1).ToArgb() == provinceMap.GetPixel(idx - 1, jdx).ToArgb())
                                            {
                                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx - 1, jdx));
                                            }
                                            else if (provinceMap.GetPixel(idx + 1, jdx).ToArgb() == provinceMap.GetPixel(idx, jdx + 1).ToArgb())
                                            {
                                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx + 1, jdx));
                                            }
                                            else if (provinceMap.GetPixel(idx + 1, jdx).ToArgb() == provinceMap.GetPixel(idx, jdx + 1).ToArgb())
                                            {
                                                provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx + 1, jdx));
                                            }
                                        }

                                        else
                                        {
                                            GenerateProvince(idx, jdx);
                                            jdx = yMapSize;
                                            idx = xMapSize;
                                            looped = 0;
                                            containsUnclaimedLand = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!(provinceMap.GetPixel(idx - 1, jdx).ToArgb() == landProvinceColor.ToArgb()))
                                    {
                                        provinceMap.SetPixel(idx, jdx, provinceMap.GetPixel(idx - 1, jdx));
                                    }
                                    else
                                    {
                                        jdx = yMapSize;
                                        idx = xMapSize;
                                        looped = 0;
                                        containsUnclaimedLand = true;
                                    }
                                }
                            }
                            else
                            {
                                jdx = yMapSize;
                                idx = xMapSize;
                                containsUnclaimedLand = true;
                            }
                                // containsUnclaimedLand = false;
                        }
                    }
                }*/
            //} while (containsUnclaimedLand);
            #endregion
            provinceMap.Save(Constants.MAP + "/provinces.bmp");
            int provinceId = 1;
            foreach (Province province in provinceList)
            {
                province.SetProvinceNo(provinceId);
                provinceId++;
            }
            foreach (Province province in seaProvinceList)
            {
                province.SetProvinceNo(provinceId);
                provinceId++;
            }
            
            Console.WriteLine("Writing definitions");
            StreamWriter definitions = File.CreateText(Constants.MAP + "/definition.csv");
            definitions.WriteLine("province;red;green;blue;x;x");

            //int totalProvincesWritten = 1;
            for (int idx = 0; idx < provinceList.Count; idx++)
            {
                definitions.WriteLine(provinceList[idx].provinceNo + ";" 
                    + provinceList[idx].provinceColor.R + ";"
                    + provinceList[idx].provinceColor.G + ";"
                    + provinceList[idx].provinceColor.B + ";"
                    + provinceList[idx].name + ";x");
              //  totalProvincesWritten ++;
                definitions.Flush();
            }
            for (int idx = 0; idx < seaProvinceList.Count; idx++)
            {
                definitions.WriteLine(seaProvinceList[idx].provinceNo + ";"
                    + seaProvinceList[idx].provinceColor.R + ";"
                    + seaProvinceList[idx].provinceColor.G + ";"
                    + seaProvinceList[idx].provinceColor.B + ";"
                    + seaProvinceList[idx].name + ";x");
                definitions.Flush();
                //totalProvincesWritten++;
            }

            definitions.Close();
            Console.WriteLine("Definitions written");
        }

       /* protected bool ExpandProvince(int xStartPixel, int yStartPixel, Color provinceColor)
        {
            currentRecursion++;
            if (currentRecursion > recursionLimit)
            {
                return false;
            }

            int direction = Random.RandomInt(0, 3);



            if ((direction == 0) && (xStartPixel + 1 < xMapSize))
            {
                if (provinceMap.GetPixel(xStartPixel + 1, yStartPixel).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel + 1, yStartPixel, provinceColor);
                    return true;
                }
                else if (provinceMap.GetPixel(xStartPixel + 1, yStartPixel).ToArgb() == provinceColor.ToArgb())
                {
                    return ExpandProvince(xStartPixel + 1, yStartPixel, provinceColor);
                }
                else
                {
                    return false;
                }
            }
            else if ((direction == 0) && (xStartPixel + 1 < xMapSize)) 
            {
                return ExpandProvince(xStartPixel, yStartPixel, provinceColor);
            }

            if (direction == 1 && (yStartPixel+1 < yMapSize))
            {
                if (provinceMap.GetPixel(xStartPixel, yStartPixel + 1).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel, yStartPixel +1, provinceColor);
                    return true;
                }
                else if (provinceMap.GetPixel(xStartPixel, yStartPixel + 1).ToArgb() == provinceColor.ToArgb())
                {
                    return ExpandProvince(xStartPixel, yStartPixel + 1, provinceColor);
                }
                else
                {
                    return false;
                }
            }
            else if ((direction == 1) && (yStartPixel + 1 < yMapSize))
            {
                return ExpandProvince(xStartPixel, yStartPixel, provinceColor);
            }
            if (direction == 2 && (xStartPixel-1 > 0))
            {
                if (provinceMap.GetPixel(xStartPixel - 1, yStartPixel).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel - 1, yStartPixel, provinceColor);
                    return true;
                }
                else if (provinceMap.GetPixel(xStartPixel - 1, yStartPixel).ToArgb() == provinceColor.ToArgb())
                {
                    return ExpandProvince(xStartPixel - 1, yStartPixel, provinceColor);
                }
                else
                {
                    return false;
                }
            }
            else if ((direction == 2) &&((xStartPixel - 1 > 0)))
            {
                return ExpandProvince(xStartPixel, yStartPixel, provinceColor);
            }

            if (direction == 3 && (yStartPixel - 1 > 0))
            {
                if (provinceMap.GetPixel(xStartPixel, yStartPixel - 1).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel, yStartPixel - 1, provinceColor);
                    return true;
                }
                else if (provinceMap.GetPixel(xStartPixel, yStartPixel - 1).ToArgb() == provinceColor.ToArgb())
                {
                   return ExpandProvince(xStartPixel, yStartPixel - 1, provinceColor);
                }
                else
                {
                    return false;
                }
            }
            else if ((direction == 3) && (yStartPixel - 1 > 0))
            {
                return ExpandProvince(xStartPixel, yStartPixel, provinceColor);
            }
            return ExpandProvince(xStartPixel, yStartPixel, provinceColor);
        }*/

        protected bool ExpandProvince(int xStartPixel, int yStartPixel, Color provinceColor)
        {
            if (currentRecursion > recursionLimit)
            {
                return false;
            }
            
            bool returnValue = false;
            if ((xStartPixel + 1) < xMapSize)
            {
                if (provinceMap.GetPixel(xStartPixel + 1, yStartPixel).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel + 1, yStartPixel, provinceColor);
                    returnValue = true;
                }
                else if (provinceMap.GetPixel(xStartPixel + 1, yStartPixel).ToArgb() == provinceColor.ToArgb())
                {
                    currentRecursion++;
                    if (ExpandProvince(xStartPixel + 1, yStartPixel, provinceColor))
                    {
                        returnValue = true;
                    }
                    currentRecursion--;
                }
            }

            if ((xStartPixel - 1) > 0)
            {
                if (provinceMap.GetPixel(xStartPixel - 1, yStartPixel).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel - 1, yStartPixel, provinceColor);
                    returnValue = true;
                }
                else if (provinceMap.GetPixel(xStartPixel - 1, yStartPixel).ToArgb() == provinceColor.ToArgb())
                {
                    currentRecursion++;
                    if (ExpandProvince(xStartPixel - 1, yStartPixel, provinceColor))
                    {
                        returnValue = true;
                    }
                    currentRecursion--;
                }
            }

            if ((yStartPixel + 1) < xMapSize)
            {
                if (provinceMap.GetPixel(xStartPixel, yStartPixel + 1).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel, yStartPixel + 1, provinceColor);
                    returnValue = true;
                }
                else if (provinceMap.GetPixel(xStartPixel, yStartPixel + 1).ToArgb() == provinceColor.ToArgb())
                {
                    currentRecursion++;
                    if (ExpandProvince(xStartPixel, yStartPixel + 1, provinceColor))
                    {
                        returnValue = true;
                    }
                    currentRecursion--;
                }
            }

            if ((yStartPixel - 1) > 0)
            {
                if (provinceMap.GetPixel(xStartPixel, yStartPixel-1).ToArgb() == landProvinceColor.ToArgb())
                {
                    provinceMap.SetPixel(xStartPixel, yStartPixel-1, provinceColor);
                    returnValue = true;
                }
                else if (provinceMap.GetPixel(xStartPixel, yStartPixel-1).ToArgb() == provinceColor.ToArgb())
                {
                    currentRecursion++;
                    if (ExpandProvince(xStartPixel, yStartPixel-1, provinceColor))
                    {
                        returnValue = true;
                    }
                    currentRecursion--;
                }
            }

            return returnValue;
        }


        protected void CreateProvince(int xCentralPixel, int yCentralPixel, Color provinceColor, Color unclaimedColor)
        {
            Console.WriteLine("Creating province " + provinceColor.ToArgb());
            if (provinceMap.GetPixel(xCentralPixel, yCentralPixel).ToArgb() == unclaimedColor.ToArgb())
            {

                int MAX_POLY_CORNERS = Random.RandomInt(10, 100);
                int IMAGE_TOP = yCentralPixel - maxProvinceSize;
                int IMAGE_BOT = yCentralPixel + maxProvinceSize;
                int IMAGE_RIGHT = xCentralPixel + maxProvinceSize;
                int IMAGE_LEFT = xCentralPixel - maxProvinceSize;

                double currentradians = 0;

                List<float> polyX = new List<float>();
                List<float> polyY = new List<float>();

                double incrementAngle = 360 / MAX_POLY_CORNERS;
                double incrementRadians = incrementAngle * (Math.PI / 180);
                int distance;
                int previousDistance = Random.RandomInt(10, maxProvinceSize);

                for (int idx = 0; idx < MAX_POLY_CORNERS; idx++)
                {
                    distance = Random.RandomInt(previousDistance - 10, previousDistance + 10);
                    polyX.Add((float)(xCentralPixel + Math.Sin(currentradians) * distance));
                    polyY.Add((float)(yCentralPixel + Math.Cos(currentradians) * distance));

                    currentradians += incrementRadians;
                    previousDistance = distance;
                    if ((polyX[idx] > 0) && (polyX[idx] < xMapSize) && (polyY[idx] > 0) && (polyY[idx] < yMapSize))
                    {
                        if (provinceMap.GetPixel((int)polyX[idx], (int)polyY[idx]).ToArgb() == unclaimedColor.ToArgb())
                        {
                            provinceMap.SetPixel((int)polyX[idx], (int)polyY[idx], provinceColor);
                        }
                    }
                    //polyX.Add(Random.RandomInt(IMAGE_LEFT, IMAGE_RIGHT));
                    //polyY.Add(Random.RandomInt(IMAGE_TOP, IMAGE_BOT));
                }

                int polyCorners = MAX_POLY_CORNERS;
                //  public-domain code by Darel Rex Finley, 2007

                int nodes, pixelX, pixelY, i, j, swap;
                int[] nodeX = new int[MAX_POLY_CORNERS];
                //  Loop through the rows of the image.
                for (pixelY = IMAGE_TOP; pixelY < IMAGE_BOT; pixelY++)
                {

                    //  Build a list of nodes.
                    nodes = 0;
                    j = polyCorners - 1;
                    for (i = 0; i < polyCorners; i++)
                    {
                        if (polyY[i] < (double)pixelY && polyY[j] >= (double)pixelY
                        || polyY[j] < (double)pixelY && polyY[i] >= (double)pixelY)
                        {
                            nodeX[nodes++] = (int)(polyX[i] + (pixelY - polyY[i]) / (polyY[j] - polyY[i])
                            * (polyX[j] - polyX[i]));
                        }
                        j = i;
                    }

                    //  Sort the nodes, via a simple “Bubble” sort.
                    i = 0;
                    while (i < nodes - 1)
                    {
                        if (nodeX[i] > nodeX[i + 1])
                        {
                            swap = nodeX[i];
                            nodeX[i] = nodeX[i + 1];
                            nodeX[i + 1] = swap;
                            if (i > 0)
                                i--;
                        }
                        else
                        {
                            i++;
                        }
                    }

                    //  Fill the pixels between node pairs.
                    for (i = 0; i < nodes; i += 2)
                    {
                        if (nodeX[i] >= IMAGE_RIGHT)
                            break;
                        if (nodeX[i + 1] > IMAGE_LEFT)
                        {
                            if (nodeX[i] < IMAGE_LEFT)
                                nodeX[i] = IMAGE_LEFT;
                            if (nodeX[i + 1] > IMAGE_RIGHT)
                                nodeX[i + 1] = IMAGE_RIGHT;
                            for (j = nodeX[i]; j < nodeX[i + 1]; j++)
                            {
                                //fillPixel(j, pixelY);
                                if ((j > 0) && (j < xMapSize) && (pixelY > 0) && (pixelY < yMapSize))
                                {
                                    if (provinceMap.GetPixel(j, pixelY).ToArgb() == unclaimedColor.ToArgb())
                                    {
                                        provinceMap.SetPixel(j, pixelY, provinceColor);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Province start already claimed, removing province from it's list");
                if (unclaimedColor.ToArgb() == landProvinceColor.ToArgb())
                {
                    for (int idx = 0; idx < provinceList.Count; idx++)
                    {
                        if (provinceList[idx].provinceColor.ToArgb() == provinceColor.ToArgb())
                        {
                            provinceList.RemoveAt(idx);
                            idx = provinceList.Count;
                        }
                    }
                }
                else if (unclaimedColor.ToArgb() == seaProvinceColor.ToArgb())
                {
                    for (int idx = 0; idx < seaProvinceList.Count; idx++)
                    {
                        if (seaProvinceList[idx].provinceColor.ToArgb() == provinceColor.ToArgb())
                        {
                            seaProvinceList.RemoveAt(idx);
                            idx = seaProvinceList.Count;
                        }

                    }
                }
                else
                {
                    Console.WriteLine("ERROR! PROVINCE NOT IN EITHER LISTS");
                }
            }


        }

        protected Province GenerateProvince(Color unclaimedProvinceColor)
        {
            int randomX;
            int randomY;
            bool checkBad = true;
            do
            {
                randomX = Random.RandomInt(0, xMapSize);
                randomY = Random.RandomInt(0, yMapSize);
                Console.WriteLine("Checking Province location ok");
                Color chosenpoint = provinceMap.GetPixel(randomX, randomY);
                if (chosenpoint.ToArgb() == unclaimedProvinceColor.ToArgb())
                {
                    checkBad = false;
                }
            } while (checkBad);
           return GenerateProvince(randomX, randomY);
        }

        protected Province GenerateProvince(int positionX, int positionY)
        {
            //choose province color. 
            Color provinceColor;
            if (provinceColorRed < 255)
            {
                provinceColor = Color.FromArgb(provinceColorRed, provinceColorGreen, provinceColorBlue);
                provinceColorRed++;
            }
            else if (provinceColorBlue < 255)
            {
                provinceColor = Color.FromArgb(provinceColorRed, provinceColorGreen, provinceColorBlue);
                provinceColorRed = 0;
                provinceColorBlue++;
            }
            else if (provinceColorGreen < 255)
            {
                provinceColor = Color.FromArgb(provinceColorRed, provinceColorGreen, provinceColorBlue);
                provinceColorRed = 0;
                provinceColorGreen++;
            }
            else
            {
                provinceColor = Color.FromArgb(provinceColorRed, provinceColorGreen, provinceColorBlue);
            }
 
            Console.WriteLine("New province of color " + provinceColor.ToArgb().ToString());
            return new Province(positionX, positionY, provinceColor);
            
        }

        protected void CreateDefaultFile()
        {
            StreamWriter defaultFile = File.CreateText(Constants.MAP + "/default.map");
            Console.WriteLine("creating default.map");
            
            defaultFile.WriteLine("width = " + xMapSize);
            defaultFile.WriteLine("height = " + yMapSize);
            defaultFile.WriteLine("max_provinces = " + (provinceList.Count + seaProvinceList.Count + 1));
            defaultFile.WriteLine("sea_starts = {");
            defaultFile.Flush();
            int looped = 0;
            foreach (Province sea in seaProvinceList)
            {
                defaultFile.Write(sea.provinceNo + " ");
                defaultFile.Flush();
                looped++;
                if (looped > 10)
                {
                    defaultFile.WriteLine();
                    looped = 0;
                }
            }
            defaultFile.WriteLine("}");
            defaultFile.WriteLine("lakes = { } ");
            defaultFile.WriteLine("definitions =" + '"' + "definition.csv" + '"');
            defaultFile.WriteLine("provinces = " + '"' + "provinces.bmp" + '"');
            defaultFile.WriteLine("positions = " + '"' + "positions.txt" + '"');
            defaultFile.WriteLine("terrain = " + '"' + "terrain.bmp" + '"');
            defaultFile.WriteLine("rivers = " + '"' + "rivers.bmp" + '"');
            defaultFile.WriteLine("terrain_definition = " + '"' + "terrain.txt" + '"');
            defaultFile.WriteLine("heightmap = " + '"' + "heightmap.bmp" +'"');
            defaultFile.WriteLine("tree_definition = " + '"' + "trees.bmp" + '"');
            defaultFile.WriteLine("continent = " + '"' + "continent.txt" + '"');
            defaultFile.WriteLine("adjacencies = " + '"' + "adjacencies.csv" + '"');
            defaultFile.WriteLine("climate = " + '"' + "climate.txt"+ '"');
            defaultFile.WriteLine("region = " + '"' + "region.txt"+ '"');
            defaultFile.WriteLine("ambient_object = " + '"' + "ambient_object.txt" + '"');
            defaultFile.WriteLine("seasons = " + '"' + "seasons.txt"+ '"');
            defaultFile.WriteLine("# Define which indices in trees.bmp palette which should count as trees for automatic terrain assignment");
            defaultFile.WriteLine("tree = { 3 4 7 10 }");
            defaultFile.Flush();
            defaultFile.Close();

            Console.WriteLine("default file written");
        }

        protected void CreatePositions()
        {
            StreamWriter positions = File.CreateText(Constants.MAP + "/positions.txt");

            foreach (Province province in provinceList)
            {
                int centralXpos = xMapSize - province.xStartCoord; //counts from bottom, rather than top. 
                int centralYpos = yMapSize - province.yStartCoord; 

                positions.WriteLine("#" + province.name);
                positions.WriteLine("   " + province.provinceNo + "=");
                positions.WriteLine("   {");
                positions.WriteLine("       position=");
                positions.WriteLine("       {");
                positions.WriteLine("           " + centralXpos + ".000 " + centralYpos + ".000 "
                    + centralXpos + ".000 " + centralYpos + ".000 "
                    + centralXpos + ".000 " + centralYpos + ".000 "
                    + centralXpos + ".000 " + centralYpos + ".000 "
                    + centralXpos + ".000 " + centralYpos + ".000 "
                    + centralXpos + ".000 " + centralYpos + ".000 ");
                positions.WriteLine("       }");
                positions.WriteLine("       rotation=");
                positions.WriteLine("       {");
                positions.WriteLine("           0.000 0.000 0.000 0.000 0.000 0.000");
                positions.WriteLine("       }");
                positions.WriteLine("       height=");
                positions.WriteLine("       {");
                positions.WriteLine("           0.000 0.000 1.000 0.000 0.000 0.000");
                positions.WriteLine("       }");
                positions.WriteLine("   }");
                positions.Flush();
            }
            positions.Close();
        }

        protected void CreateContinents()
        {
            StreamWriter continents = File.CreateText(Constants.MAP + "/continent.txt");
            continents.WriteLine("europe = { }");
            continents.WriteLine("asia = { }");
            continents.WriteLine("africa = { }");
            continents.WriteLine("north_america = { }");
            continents.WriteLine("south_america = { }");
            continents.WriteLine("oceania = { }");
            continents.Flush();
            continents.WriteLine(NameGenerator.GenerateName() + " = {");
            foreach (Province province in provinceList)
            {
                if (province.xStartCoord < (xMapSize / 2) && province.yStartCoord < (yMapSize/2))
                {
                    continents.Write(province.provinceNo + " ");
                    continents.Flush();
                }
            }
            continents.WriteLine("}");

            continents.WriteLine(NameGenerator.GenerateName() + " = {");
            foreach (Province province in provinceList)
            {
                if (province.xStartCoord > (xMapSize / 2) && province.yStartCoord < (yMapSize / 2))
                {
                    continents.Write(province.provinceNo + " ");
                    continents.Flush();
                }
            }
            continents.WriteLine("}");

            continents.WriteLine(NameGenerator.GenerateName() + " = {");
            foreach (Province province in provinceList)
            {
                if (province.xStartCoord < (xMapSize / 2) && province.yStartCoord > (yMapSize / 2))
                {
                    continents.Write(province.provinceNo + " ");
                    continents.Flush();
                }
            }
            continents.WriteLine("}");
            continents.WriteLine(NameGenerator.GenerateName() + " = {");
            foreach (Province province in provinceList)
            {
                if (province.xStartCoord > (xMapSize / 2) && province.yStartCoord > (yMapSize / 2))
                {
                    continents.Write(province.provinceNo + " ");
                    continents.Flush();
                }
            }
            continents.WriteLine("}");
            continents.Flush();
            continents.Close();
        }
    }

    class Province
    {
        public static int totalProvinceNo { get; private set; }
        public Color provinceColor { get; private set; }
        public int xStartCoord { get; private set; }
        public int yStartCoord { get; private set; }
        public string name { get; private set; }
        public int provinceNo { get; private set; }

        public Province(int xCoord, int yCoord, Color color)
        {
            xStartCoord = xCoord;
            yStartCoord = yCoord;
            provinceColor = color;
            name = NameGenerator.GenerateName();
            this.provinceNo = totalProvinceNo++;
        }
        public void SetProvinceNo(int provinceNo)
        {
            this.provinceNo = provinceNo;
        }
    }
}
