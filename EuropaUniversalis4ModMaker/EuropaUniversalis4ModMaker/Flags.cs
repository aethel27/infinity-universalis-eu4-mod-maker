﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using ImageMagick;

namespace EuropaUniversalis4ModMaker
{
    class Flags
    {
       // protected List<string> flagNames = new List<string>();
        protected List<Bitmap> flagTemplates = new List<Bitmap>();

        Color setColor1, setColor2, setColor3, setColor4, setColor5;

        public Flags()
        {
            /*string[] flagFileNames = Directory.GetFiles(Constants.INSTALLPATH + "/gfx/flags");
            Console.WriteLine("Loading in old flags");
            foreach (string filname in flagFileNames)
            {
                flagNames.Add(filname);
            }*/

            string[] flagTemplateFileNames = Directory.GetFiles(@"FlagTemplates");
            foreach (string filename in flagTemplateFileNames)
            {
                flagTemplates.Add(new Bitmap(filename));
            }
            Console.WriteLine("Loading flag templates");

            setColor1 = Color.FromArgb(0, 0, 0);
            setColor2 = Color.FromArgb(255, 255, 255);
            setColor3 = Color.FromArgb(122, 122, 122);
            setColor4 = Color.FromArgb(0, 122, 255);
            setColor5 = Color.FromArgb(255, 122, 0);

        }

        public void GenerateFlag(string TagId)
        {
            //Generate random colors for flag.
            
            Color color1 = GenerateNewColor();
            Color color2 = GenerateNewColor();
            Color color3 = GenerateNewColor();
            Color color4 = GenerateNewColor();
            Color color5 = GenerateNewColor();

            //select a flag template. 
            int templateNo = Random.RandomInt(0, flagTemplates.Count);

            Bitmap countryFlag = flagTemplates[templateNo];

            Console.WriteLine("Creating flag for " + TagId);

            for (int idx = 0; idx < countryFlag.Width; idx++)
            {
                for (int jdx = 0; jdx < countryFlag.Height; jdx++)
                {
                    //replace template holding values with new colors. 
                    if (countryFlag.GetPixel(idx, jdx) == setColor1)
                    {
                        countryFlag.SetPixel(idx, jdx, color1);
                    }
                    else if (countryFlag.GetPixel(idx, jdx) == setColor2)
                    {
                        countryFlag.SetPixel(idx, jdx, color2);
                    }
                    else if (countryFlag.GetPixel(idx, jdx) == setColor3)
                    {
                        countryFlag.SetPixel(idx, jdx, color3);
                    }
                    else if (countryFlag.GetPixel(idx, jdx) == setColor4)
                    {
                        countryFlag.SetPixel(idx, jdx, color4);
                    }
                    else if (countryFlag.GetPixel(idx, jdx) == setColor5)
                    {
                        countryFlag.SetPixel(idx, jdx, color5);
                    }
                }
            }

            string destName = Constants.GFX + "/flags";

            Console.WriteLine("Writing image for " + TagId);

            //Use Magick to write to targa, as c# and .net has no native handling. 
            MagickImage image = new MagickImage(countryFlag);

            Stream outputFile = File.Create(System.IO.Path.Combine(destName, TagId + ".tga"));
            image.Write(outputFile);

            outputFile.Close();

            //temporary assignment function. 
            //string installPath = "C:/Program Files (x86)/Steam/steamapps/common/Europa Universalis IV/gfx/flags";
            //string oldFlagName = "AFG.tga";
            /*
            destName = Constants.GFX + "/flags";
            int sourceNo = Random.RandomInt(0, flagNames.Count);


            string source = flagNames[sourceNo];
            Console.WriteLine("Removing flag " + flagNames[sourceNo] + "from the list");
            flagNames.RemoveAt(sourceNo);
            

            string dest = System.IO.Path.Combine(destName, TagId + ".tga");

            System.IO.File.Copy(source, dest, true);*/

        }

        private Color GenerateNewColor()
        {
            switch (Random.RandomInt(1, 31))
            {
                case 1:
                    int red = GenerateTint();
                    int green = GenerateTint();
                    int blue = GenerateTint();

                    return Color.FromArgb(red, green, blue);

                case 2: return Color.ForestGreen;
                case 3: return Color.Red;
                case 4: return Color.Yellow;
                case 5: return Color.Green;
                case 6: return Color.Brown;
                case 7: return Color.Black;
                case 8: return Color.Blue;
                case 9: return Color.Aquamarine;
                case 10: return Color.White;
                case 11: return Color.RosyBrown;
                case 12: return Color.RoyalBlue;
                case 13: return Color.SandyBrown;
                case 14: return Color.Silver;
                case 15: return Color.Gold;
                case 16: return Color.Purple;
                case 17: return Color.Plum;
                case 18: return Color.Chocolate;
                case 19: return Color.DarkBlue;
                case 20: return Color.DarkGreen;
                case 21: return Color.DarkOliveGreen;
                case 22: return Color.Olive;
                case 23: return Color.OrangeRed;
                case 24: return Color.SaddleBrown;
                case 25: return Color.SeaGreen;
                case 26: return Color.SkyBlue;
                case 27: return Color.SpringGreen;
                case 28: return Color.YellowGreen;
                case 29: return Color.Tomato;
                case 30: return Color.SteelBlue;

                default: return GenerateNewColor();
            }
        }
        private int GenerateTint()
        {
            float percentile = Random.RandomFloat();

            if (percentile < 0.25f)
            {
                return Random.RandomInt(61, 182);
            }
            else if (percentile >= 0.25f && percentile < 0.75f)
            {
                return Random.RandomInt(0, 122);
            }
            else
            {
                return Random.RandomInt(122, 256);
            }
        }
    }
}
